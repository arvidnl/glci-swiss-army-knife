Small tests:

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml -s plain merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml -s plain focus sanity
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh

  $ ../bin/main.exe -s double-quoted --ci-file ../test_confs/small/.gitlab-ci-small.yml focus sanity
  "sanity":
    "image": "${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}"
    "script":
    - "scripts/check_opam_test.sh"


  $ ../bin/main.exe -s plain --ci-file ../test_confs/small/.gitlab-ci-defaults.yml merge -t expand
  default:
    before_script: default-before-script.sh
    retry: 2
  unit-test-job:
    before_script: default-before-script.sh
    script: unit-test.sh
    retry: 0

  $ ../bin/main.exe -s plain --ci-file ../test_confs/small/.gitlab-ci-defaults.yml merge -t no-expansion
  default:
    before_script: default-before-script.sh
    retry: 2
  unit-test-job:
    script: unit-test.sh
    retry: 0

  $ ../bin/main.exe -s plain --ci-file ../test_confs/small/.gitlab-ci-defaults.yml merge -t debug-expansion
  default:
    before_script: default-before-script.sh
    retry: 2
  unit-test-job:
    __gci_default_begin__: begin
    before_script: default-before-script.sh
    __gci_default___end__: end
    script: unit-test.sh
    retry: 0
