Test settings file

Test gitlab access token & gitlab_project

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  The 'lint' action requires you to set a GitLab Access token (https://gitlab.com/-/profile/personal_access_tokens) either through '--gl-access-token' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]
  $ echo '{ "gl-access-token": "glpat-..." }' > settings.json
  $ ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  The 'lint' action requires you to specify a GitLab project towards whose API the lint query is executed. This is set either through '--gl-project' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]
  $ echo '{ "gl-project": 123 }' > settings.json
  $ OCAMLRUNPARAM=b=0 ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  Fatal error: exception Failure("Expected 'gl-project' to be a string in settings.json")
  [2]
  $ echo '{ "gl-access-token": [] }' > settings.json
  $ OCAMLRUNPARAM=b=0 ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  Fatal error: exception Failure("Expected 'gl-access-token' to be a string in settings.json")
  [2]

Test scalar styles

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  The 'lint' action requires you to set a GitLab Access token (https://gitlab.com/-/profile/personal_access_tokens) either through '--gl-access-token' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]
  $ echo '{ "scalar-style": "plain" }' > settings.json
  $ ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
  $ echo '{ "scalar-style": "double-quoted" }' > settings.json
  $ ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  "sanity":
    "image": "${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}"
    "script":
    - "scripts/check_opam_test.sh"

Test environments

  $ echo '{ "environments": { "foo": { }, "bar": { "baz": "toto" } } }' > settings.json
  $ ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml show environments -f verbose
  Available environments:
  Environment 'foo':
  Environment 'bar':
   - baz: 'toto'

Test loading settings from the environment

  $ GCI_GL_ACCESS_TOKEN="glpat-..." ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  The 'lint' action requires you to specify a GitLab project towards whose API the lint query is executed. This is set either through '--gl-project' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]

  $ GCI_SCALAR_STYLE=plain ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
  $ GCI_SCALAR_STYLE=double-quoted ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  "sanity":
    "image": "${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}"
    "script":
    - "scripts/check_opam_test.sh"
  $ OCAMLRUNPARAM=b=0 GCI_SCALAR_STYLE=foobar ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  Fatal error: exception Failure("Expected 'GCI_SCALAR_STYLE' to be one of plain, single-quoted, any, double-quoted, folded, literal")
  [2]

  $ GCI_ENVIRONMENTS='{ "foo": { }, "bar": { "baz": "toto" } }' ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml show environments -f verbose
  Available environments:
  Environment 'foo':
  Environment 'bar':
   - baz: 'toto'
  $ OCAMLRUNPARAM=b=0 GCI_ENVIRONMENTS='1234' ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml show environments -f verbose
  Fatal error: exception Failure("Unexpected format of GCI_ENVIRONMENTS: not assoc")
  [2]

Test priorities (global args > environment variables > settings file):

  $ echo '{ "scalar-style": "double-quoted" }' > settings.json
  $ GCI_SCALAR_STYLE=plain ../bin/main.exe --settings settings.json --scalar-style folded --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  "sanity":
    "image": >-
      ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    "script":
    - >-
      scripts/check_opam_test.sh
  $ GCI_SCALAR_STYLE=plain ../bin/main.exe --settings settings.json --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh

Test loading invalid settings:

  $ OCAMLRUNPARAM=b=0 ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml --settings DOES_NOT_EXIST.json lint
  Fatal error: exception Failure("Specified configuration file 'DOES_NOT_EXIST.json' does not exist")
  [2]
