First, we create a simple pipeline configuration with three jobs using
[changes:] clauses:

  $ cat <<'EOT' > .gitlab-ci.yml
  > ---
  > stages: [test]
  > job_with_changes__foo_md:
  >   rules:
  >     - changes:
  >         - 'foo.md'
  >   script: example.sh
  > job_with_changes__root_txt:
  >   rules:
  >     - changes:
  >         - '*.txt'
  >   script: example.sh
  > job_with_changes__all_ml:
  >   rules:
  >     - changes:
  >         - '**/*.ml'
  >   script: example.sh
  > EOT

Define a helper funciton 'simulate_pipeline' to write a settings file
containing one environment 'test-env' with a given set of changes, and
then running a simulation against that environment:

  $ simulate_pipeline() {
  >  echo "{ \"environments\": { \"test-env\": { \"CI_PIPELINE_SOURCE\": \"merge_request_event\", \"__changes\": $1 } } }" > .gci.json;
  >  ../bin/main.exe simulate pipeline in test-env
  > }

Test the 'simulate pipeline' command:

  $ simulate_pipeline '[]'
  [CI Pipeline]
  └──[test]
  $ simulate_pipeline '["foo.md"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__foo_md
  $ simulate_pipeline '["bar.md"]'
  [CI Pipeline]
  └──[test]
  $ simulate_pipeline '["test1.txt"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__root_txt
  $ simulate_pipeline '["test2.txt"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__root_txt
  $ simulate_pipeline '["foobar/test2.txt"]'
  [CI Pipeline]
  └──[test]
  $ simulate_pipeline '["foo/bar.ml"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__all_ml
  $ simulate_pipeline '["foo/gaz/bar.ml"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__all_ml
  $ simulate_pipeline '["main.ml"]'
  [CI Pipeline]
  └──[test]
     └── [on_success] job_with_changes__all_ml

We do the same using the 'simulate job' command:

  $ simulate_job() {
  >  echo "{ \"environments\": { \"test-env\": { \"CI_PIPELINE_SOURCE\": \"merge_request_event\", \"__changes\": $1 } } }" > .gci.json;
  >  ../bin/main.exe simulate job $2 in test-env
  > }
  $ simulate_job '[]' job_with_changes__foo_md
  Job job_with_changes__foo_md is not triggered by rule <none>
  $ simulate_job '["foo.md"]' job_with_changes__foo_md
  Job 'job_with_changes__foo_md' is triggered 'on_success' by rule 0:{changes: [foo.md]}
  $ simulate_job '["bar.md"]' job_with_changes__foo_md
  Job job_with_changes__foo_md is not triggered by rule <none>
  $ simulate_job '["test1.txt"]' job_with_changes__root_txt
  Job 'job_with_changes__root_txt' is triggered 'on_success' by rule 0:{changes: ['*.txt']}
  $ simulate_job '["test2.txt"]' job_with_changes__root_txt
  Job 'job_with_changes__root_txt' is triggered 'on_success' by rule 0:{changes: ['*.txt']}
  $ simulate_job '["foobar/test2.txt"]' job_with_changes__root_txt
  Job job_with_changes__root_txt is not triggered by rule <none>
  $ simulate_job '["foo/bar.ml"]' job_with_changes__all_ml
  Job 'job_with_changes__all_ml' is triggered 'on_success' by rule 0:{changes: ['**/*.ml']}
  $ simulate_job '["foo/gaz/bar.ml"]' job_with_changes__all_ml
  Job 'job_with_changes__all_ml' is triggered 'on_success' by rule 0:{changes: ['**/*.ml']}
  $ simulate_job '["main.ml"]' job_with_changes__all_ml
  Job 'job_with_changes__all_ml' is triggered 'on_success' by rule 0:{changes: ['**/*.ml']}


A GitLab job rule can combine [changes:] clauses with [if:] clauses
The job is triggered only when both clauses evaluate to true.

  $ cat <<'EOT' > .gitlab-ci.yml
  > ---
  > stages: [test]
  > multiple_clauses:
  >   rules:
  >     - if: '$VAR == "1"'
  >       changes:
  >         - 'foo.md'
  >   script: example.sh
  > EOT
  $ simulate_job() {
  >  echo "{ \"environments\": { \"test-env\": { \"CI_PIPELINE_SOURCE\": \"merge_request_event\", \"__changes\": $1 $2 } } }" > .gci.json;
  >  ../bin/main.exe simulate job multiple_clauses in test-env
  > }
  $ simulate_job '[]' ', "VAR": "1"'
  Job multiple_clauses is not triggered by rule <none>
  $ simulate_job '["foo.md"]' ', "VAR": ""'
  Job multiple_clauses is not triggered by rule <none>
  $ simulate_job '["foo.md"]' ', "VAR": "1"'
  Job 'multiple_clauses' is triggered 'on_success' by rule 0:{if: $VAR == "1", changes: [foo.md]}


The [changes:] clause allows a more complete form that specifies a
[compare_to:]. It is has no impact on simulations (we always compare
to the changes given by the environment), but we must parse it:


  $ cat <<'EOT' > .gitlab-ci.yml
  > ---
  > stages: [test]
  > full_changes:
  >   rules:
  >     - changes:
  >         compare_to: master
  >         paths:
  >           - 'foo.md'
  >     - changes:
  >         paths:
  >           - 'bar.md'
  >   script: example.sh
  > EOT
  $ simulate_job() {
  >  echo "{ \"environments\": { \"test-env\": { \"CI_PIPELINE_SOURCE\": \"merge_request_event\", \"__changes\": $1 } } }" > .gci.json;
  >  ../bin/main.exe simulate job full_changes in test-env
  > }
  $ simulate_job '[]'
  Job full_changes is not triggered by rule <none>
  $ simulate_job '["foo.md"]'
  Job 'full_changes' is triggered 'on_success' by rule 0:{changes: {compare_to: master, paths: [foo.md]}}
  $ simulate_job '["bar.md"]'
  Job 'full_changes' is triggered 'on_success' by rule 1:{changes: {paths: [bar.md]}}

Note, as documented here:
https://docs.gitlab.com/ee/ci/yaml/#ruleschanges, that [changes:]
clauses vacuously evaluate to true in certains pipelines where there
is no specific change set associated to it. GitLab CI Inspector
simulates these rules and emits a warning when a [changes:] clause
vacuously evaluates to true:


  $ cat <<'EOT' > .gitlab-ci.yml
  > ---
  > stages: [test]
  > change_no_compare_to:
  >   rules:
  >     - changes: ['bar.md']
  >   script: example.sh
  > change_with_compare_to:
  >   rules:
  >     - changes:
  >         paths: ['bar.md']
  >         compare_to: 'master'
  >   script: example.sh
  > EOT
  $ simulate_job() {
  >  echo "{ \"environments\": { " > .gci.json
  >  echo "\"merge_request\": { \"CI_PIPELINE_SOURCE\": \"merge_request_event\", \"__changes\": $1 }, " >> .gci.json
  >  echo "\"branch_pipeline\": { \"CI_PIPELINE_SOURCE\": \"push\", \"CI_COMMIT_BRANCH\": \"foo\", \"__changes\": $1 }, " >> .gci.json
  >  echo "\"tag_pipeline\": { \"CI_PIPELINE_SOURCE\": \"push\", \"CI_COMMIT_TAG\": \"foo\", \"__changes\": $1 } " >> .gci.json
  >  echo "} }" >> .gci.json;
  >  ../bin/main.exe simulate job $2 in $3
  > }

Consequently, we get a warning for vacuous evaluations in pipelines:

  $ simulate_job '["bar.md"]' change_no_compare_to merge_request
  Job 'change_no_compare_to' is triggered 'on_success' by rule 0:{changes: [bar.md]}
  $ simulate_job '["bar.md"]' change_no_compare_to branch_pipeline
  Job 'change_no_compare_to' is triggered 'on_success' by rule 0:{changes: [bar.md]}
  $ simulate_job '["bar.md"]' change_no_compare_to tag_pipeline
  Warning: The [changes:] clause in rule 0:{changes: [bar.md]} of job 'change_no_compare_to' vacuously evaluates to true in this environment. See the README.md for more info.
  Job 'change_no_compare_to' is triggered 'on_success' by rule 0:{changes: [bar.md]}

Unless, a [compare_to:] clause has been set:

  $ simulate_job '["bar.md"]' change_with_compare_to merge_request
  Job 'change_with_compare_to' is triggered 'on_success' by rule 0:{changes: {paths: [bar.md], compare_to: master}}
  $ simulate_job '["bar.md"]' change_with_compare_to branch_pipeline
  Job 'change_with_compare_to' is triggered 'on_success' by rule 0:{changes: {paths: [bar.md], compare_to: master}}
  $ simulate_job '["bar.md"]' change_with_compare_to tag_pipeline
  Job 'change_with_compare_to' is triggered 'on_success' by rule 0:{changes: {paths: [bar.md], compare_to: master}}
