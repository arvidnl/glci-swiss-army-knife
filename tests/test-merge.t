
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml merge --exclude-untriggered-jobs
  The option '--exclude-untriggered-jobs' is incompatible with '--evaluation-strategy always-include' and '--evaluation-strategy never-include'. Please either set '--evaluation-strategy <ENV>' or '--evaluation-strategy empty-env'
  [1]
