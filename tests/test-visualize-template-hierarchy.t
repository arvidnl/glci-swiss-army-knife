

  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/diamond.yml visualize templates --no-prune
  .template
  ├──job_a
  │  └──job_c
  └──job_b
     └──job_c
  job_b
  └──job_c
  job_a
  └──job_c
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/diamond.yml visualize templates --no-prune job_a
  job_a
  └──job_c
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/diamond.yml visualize templates --no-prune --reverse
  job_c
  ├──job_a
  │  └──.template
  └──job_b
     └──.template
  job_b
  └──.template
  job_a
  └──.template
  .template
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/diamond.yml visualize templates --no-prune --reverse job_c
  job_c
  ├──job_a
  │  └──.template
  └──job_b
     └──.template
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/missing.yml visualize templates --no-prune
  Warning: Job 'job_c' extends 'job_d', but no such job or template is defined -- it will be ignored.
  .template
  ├──job_a
  │  └──job_c
  └──job_b
     └──job_c
  job_b
  └──job_c
  job_a
  └──job_c
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/cycle.yml visualize templates --no-prune
  A cycle exists in template extensions: job_b -> job_a -> job_d -> job_c -> job_b
  [1]
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/filter.yml visualize templates --no-prune
  .template
  ├──job_a
  │  └──job_c
  └──job_b
     └──job_c
  job_b
  └──job_c
  job_a
  └──job_c
  .other_template
  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/multiple_inheritance.yml visualize templates --no-prune
  job_b
  └──job_c
  job_a
  └──job_c

Test pruning. The job trees job_b and job_a are not considered roots
as they're already present in the tree starting at the root .template.

  $ ../bin/main.exe --ci-file ../test_confs/template-hiearchy/diamond.yml visualize templates
  .template
  ├──job_a
  │  └──job_c
  └──job_b
     └──job_c
