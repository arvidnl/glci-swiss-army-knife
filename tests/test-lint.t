Only test errors conditions for to avoid having to supply an GitLab
access token for the test suite.

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-defaults.yml lint
  The 'lint' action requires you to set a GitLab Access token (https://gitlab.com/-/profile/personal_access_tokens) either through '--gl-access-token' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]
  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-defaults.yml --gl-access-token FOOBAR lint
  The 'lint' action requires you to specify a GitLab project towards whose API the lint query is executed. This is set either through '--gl-project' or in a '.gci.json' file (see '../bin/main.exe man' for more on '.gci.json').
  [1]
