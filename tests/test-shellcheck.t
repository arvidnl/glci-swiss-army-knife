Small tests:

  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-script.yml shellcheck job test_script_error | grep -v 'In .*test_script_error.sh '
  Job `test_script_error` failed shellcheck.
  Shellchecking job `test_script_error`: 
  if foobar; echo "bar"; fi
  ^-- SC1049 (error): Did you forget the 'then' for this 'if'?
  ^-- SC1073 (error): Couldn't parse this if expression. Fix to allow more checks.
                         ^-- SC1050 (error): Expected 'then'.
                           ^-- SC1072 (error): Unexpected keyword/token. Fix any mentioned problems and try again.
  
  For more information:
    https://www.shellcheck.net/wiki/SC1049 -- Did you forget the 'then' for thi...
    https://www.shellcheck.net/wiki/SC1050 -- Expected 'then'.
    https://www.shellcheck.net/wiki/SC1072 -- Unexpected keyword/token. Fix any...


  $ ../bin/main.exe --ci-file ../test_confs/small/.gitlab-ci-script.yml shellcheck all jobs | grep -v 'In .*test_script_error.sh '
  Following jobs failed shellcheck:
  test_script_error
  Shellchecking job `test_script1`.
  Shellchecking job `test_script2`.
  Shellchecking job `test_script3`.
  Shellchecking job `test_script4`.
  Shellchecking job `test_script_error`.
  
  if foobar; echo "bar"; fi
  ^-- SC1049 (error): Did you forget the 'then' for this 'if'?
  ^-- SC1073 (error): Couldn't parse this if expression. Fix to allow more checks.
                         ^-- SC1050 (error): Expected 'then'.
                           ^-- SC1072 (error): Unexpected keyword/token. Fix any mentioned problems and try again.
  
  For more information:
    https://www.shellcheck.net/wiki/SC1049 -- Did you forget the 'then' for thi...
    https://www.shellcheck.net/wiki/SC1050 -- Expected 'then'.
    https://www.shellcheck.net/wiki/SC1072 -- Unexpected keyword/token. Fix any...
  Shellchecking job `test_script_var_no_err`.
