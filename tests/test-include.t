  $ ../bin/main.exe --ci-file ../test_confs/conditional-includes/.gitlab-ci.yml -s plain merge -e 'empty-env'
  job1:
    script: echo "job1"
  job2:
    needs:
    - job1
    script: echo "job1"

  $ ../bin/main.exe --ci-file ../test_confs/nested-includes/.gitlab-ci.yml -s plain merge
  default:
    after_script:
    - echo "Job complete."


  $ ../bin/main.exe --ci-file ../test_confs/basic-includes/.gitlab-ci-string.yml -s plain merge
  job:
    script: echo "script"

  $ ../bin/main.exe --ci-file ../test_confs/basic-includes/.gitlab-ci-typed.yml -s plain merge
  job:
    script: echo "script"

  $ ../bin/main.exe --ci-file ../test_confs/basic-includes/.gitlab-ci-array.yml -s plain merge
  job_a:
    script: echo "job a"
  job_b:
    script: echo "job b"

  $ ../bin/main.exe --ci-file ../test_confs/basic-includes/.gitlab-ci-array-typed.yml -s plain merge
  job_a:
    script: echo "job a"
  job_b:
    script: echo "job b"

  $ ../bin/main.exe --ci-file ../test_confs/basic-includes/.gitlab-ci-array-mixed.yml -s plain merge
  job_a:
    script: echo "job a"
  job_b:
    script: echo "job b"

  $ ../bin/main.exe --ci-file ../test_confs/nested-includes/.gitlab-ci-duplicates.yml -s plain merge
  default:
    before_script: default-before-script.sh
    retry: 2
  unit-test-job:
    script: unit-test.sh
    retry: 0
  smoke-test-job:
    script: smoke-test.sh


