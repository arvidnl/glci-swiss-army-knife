Test the run-pipeline utility.

  $ export GCI=../bin/main.exe
  $ export GCI_UTILS_DIR="../utils"
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run
  GitLab token has not been supplied through --gl-access-token or GCI_GL_ACCESS_TOKEN. See ../utils/run-pipeline.sh --help.
  [1]
  $ export GCI_GL_ACCESS_TOKEN="glpat-..."
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run
  GitLab project has not been supplied through --gl-project or GCI_GL_PROJECT, and could not be inferred. See ../utils/run-pipeline.sh --help.
  [1]
  $ export GCI_GL_PROJECT="arvidnl/gitlab-ci-inspector"
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run
  Target branch has not been supplied through --branch or BRANCH, and could not be inferred. See ../utils/run-pipeline.sh --help.
  [1]
  $ export BRANCH="arvid@topic"
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run
  Creating pipeline on branch 'arvid@topic' in project 'arvidnl/gitlab-ci-inspector'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic",
    "variables": []
  } https://gitlab.com/api/v4/projects/arvidnl%2Fgitlab-ci-inspector/pipeline
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run FOO=BAR TOTO=TATA
  Creating pipeline on branch 'arvid@topic' in project 'arvidnl/gitlab-ci-inspector'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic",
    "variables": [
      {
        "key": "FOO",
        "value": "BAR"
      },
      {
        "key": "TOTO",
        "value": "TATA"
      }
    ]
  } https://gitlab.com/api/v4/projects/arvidnl%2Fgitlab-ci-inspector/pipeline
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run --branch "arvid@topic2"
  Creating pipeline on branch 'arvid@topic2' in project 'arvidnl/gitlab-ci-inspector'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic2",
    "variables": []
  } https://gitlab.com/api/v4/projects/arvidnl%2Fgitlab-ci-inspector/pipeline
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run --gl-project "arvidnl/test-project"
  Creating pipeline on branch 'arvid@topic' in project 'arvidnl/test-project'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic",
    "variables": []
  } https://gitlab.com/api/v4/projects/arvidnl%2Ftest-project/pipeline
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run --gl-project "arvidnl/test-project"
  Creating pipeline on branch 'arvid@topic' in project 'arvidnl/test-project'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic",
    "variables": []
  } https://gitlab.com/api/v4/projects/arvidnl%2Ftest-project/pipeline
  $ cat <<EOT > .gitlab-ci.yml
  > test_job:
  >   script: echo "Test!"
  > EOT
  $ cat <<EOT > .gci.json
  > {
  >   "environments": {
  >     "master": {
  >       "CI_PIPELINE_SOURCE": "push",
  >       "CI_COMMIT_BRANCH": "master"
  >     }
  >   }
  > }
  $ ../scripts/docker-entrypoint.sh run-pipeline --dry-run --env master
  Creating pipeline on branch 'arvid@topic' in project 'arvidnl/gitlab-ci-inspector' in environment 'master'...
  curl --silent --request POST --header Content-Type: application/json --header Private-Token: glpat-... --data {
    "ref": "arvid@topic",
    "variables": [
      {
        "key": "CI_COMMIT_BRANCH",
        "value": "master"
      },
      {
        "key": "CI_PIPELINE_SOURCE",
        "value": "push"
      }
    ]
  } https://gitlab.com/api/v4/projects/arvidnl%2Fgitlab-ci-inspector/pipeline

Test with an invalid argument:

  $ ../scripts/docker-entrypoint.sh run-pipeline master
  Unrecognized argument 'master', see --help.
  [1]
