#!/bin/sh

set -eu

utils_dir=${GCI_UTILS_DIR:-utils}
if [ ! -d "$utils_dir" ]; then
  cat << EOT
Cannot find GCI utilities directory (expected at '$utils_dir'). Try
setting the environment variable GCI_UTILS_DIR to point to the 'utils'
sub-directory in the folder where gitlab-ci-inspector is installed.
EOT
  exit 1
fi

utils=$(find "$utils_dir" -iname \*.inc.sh -prune -o -iname \*.sh -print | while read -r util; do basename "${util%%.sh}"; done)

if [ "${1:-}" = "--help" ] || [ $# -lt 1 ]; then
  cat << EOT
GitLab CI Inspector entrypoint.

If the first argument to the entrypoint corresponds to one of the
utility scripts:

EOT
  for util in ${utils}; do
    echo " - $util"
  done
  cat << EOT

Then this script is executed and any remaining arguments are passed to
the utility. For more information on each utility, call it with
'--help', e.g.:

  docker run gitlab-ci-inspector simulate-all-environments --help

If the first argument does not correspond to a utility script, the it
and remaining arguments are passed directly to the GitLab CI Inspector
binary.

Calling the entrypoint with the argument '--help' displays this message. To see the
help for 'gitlab-ci-inspector', pass the argument 'man' instead.

If the environment variable 'GCI_GIT_AUTO_SAFE' is set to 'true', then the
current working directory is added to the git's set of 'safe.directory'.
This is potentially unsafe.
EOT

  exit 0
fi

arg="${1:-}"
shift
if [ -f "${utils_dir}/${arg}.sh" ]; then
  # Mark the current directory as safe for git.
  #
  # When the GCI image is executed with a --volume mapping, then that
  # mapping will typically not have the same uid/gid as the host's
  # user. Then git will complain about .git directories being unsafe.
  # This works around this issue. This also means that the GCI image
  # should not be executed with a volume mapping to an untrusted
  # directory.
  if [ "${GCI_GIT_AUTO_SAFE:-}" = "true" ]; then
    git config --global --add safe.directory "$(pwd)"
  fi

  "${utils_dir}/${arg}.sh" "$@"
else
  gitlab-ci-inspector "${arg}" "$@"
fi
