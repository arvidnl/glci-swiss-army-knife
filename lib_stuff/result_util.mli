(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

module Syntax : sig
  (** Syntax for {!Result.bind} *)
  val ( let* ) : ('a, 'b) result -> ('a -> ('c, 'b) result) -> ('c, 'b) result

  (** Syntax for {!Result.ok} *)
  val return : 'a -> ('a, 'b) result

  (** Syntax for {!Result.error} *)
  val fail : 'b -> ('a, 'b) result

  (** Applicative bind *)
  val ( let+ ) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result
end

(** [list_map_e] is a Result-aware variant of {!List.map}. *)
val list_map_e : ('a -> ('b, 'c) result) -> 'a list -> ('b list, 'c) result
