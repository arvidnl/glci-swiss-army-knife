let filter_mapi f xs =
  let rec aux i = function
    | [] -> []
    | x :: xs -> (
        let head = f i x in
        let tl = aux (succ i) xs in
        match head with Some x' -> x' :: tl | None -> tl)
  in
  aux 0 xs
