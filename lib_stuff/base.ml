(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** {2 Common structures} *)

module String_set = struct
  include Set.Make (String)

  let pp fmt set =
    if is_empty set then Format.fprintf fmt "{}"
    else
      Format.fprintf
        fmt
        "@[<hov 2>{ %a }@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           (fun fmt -> Format.fprintf fmt "%S"))
        (elements set)
end

let with_open_out file write_f =
  let chan = open_out file in
  try
    write_f chan ;
    close_out chan
  with x ->
    close_out chan ;
    raise x

let write_file filename ~contents =
  with_open_out filename @@ fun ch -> output_string ch contents

let error fmt =
  Format.kasprintf
    (fun s ->
      prerr_endline s ;
      exit 1)
    fmt
