(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

module String_set : sig
  include Set.S with type elt = string

  (** Pretty-print a set of strings.

      Items are quoted, separated by commas and breakable spaces,
      and the result is surrounded by braces. *)
  val pp : Format.formatter -> t -> unit
end

(** Write a string into a file, overwriting the file if it already exists.

    Usage: [write_file filename ~contents] *)
val write_file : string -> contents:string -> unit

(** TODO *)
val error : ('a, Format.formatter, unit, 'b) format4 -> 'a
