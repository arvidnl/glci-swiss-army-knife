let join m1 m2 = match m1 with None -> m2 | _ -> m1

module Syntax = struct
  let ( let* ) = Option.bind

  let ( >>= ) o f = Option.bind o f

  let ( <|> ) = join

  let return = Option.some

  let fail _ = Option.none

  let ( let+ ) m f =
    let* v = m in
    return (f v)
end

let concat ls =
  let rec loop = function
    | [] -> Some []
    | Some v :: ls -> (
        match loop ls with None -> None | Some ls' -> Some (v :: ls'))
    | None :: _ -> None
  in
  loop ls

let concat_assoc ls =
  let rec loop = function
    | [] -> Some []
    | (k, Some v) :: ls -> (
        match loop ls with None -> None | Some ls' -> Some ((k, v) :: ls'))
    | (_, None) :: _ -> None
  in
  loop ls
