(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

module Syntax = struct
  let ( let* ) = Result.bind

  let return = Result.ok

  let fail = Result.error

  let ( let+ ) m f =
    let* v = m in
    return (f v)
end

let list_rev_map_e f l =
  let open Syntax in
  let rec aux ys = function
    | [] -> return ys
    | x :: xs ->
        let* y = f x in
        (aux [@ocaml.tailcall]) (y :: ys) xs
  in
  aux [] l

let list_map_e f l = list_rev_map_e f l |> Result.map List.rev
