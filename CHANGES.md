# Changelog

## Development Version

### Breaking Changes

- The 'lint' action now requires the specifying a GitLab project
  (e.g. `nomadic-labs/gitlab-ci-inspector`) which will be
  specified in the [GitLab lint
  API](https://docs.gitlab.com/ee/api/lint.html) query. This is done
  either through the `--gl-project` CLI argument or through the
  `.gci.json` settings file.
- Renamed tool to `gitlab-ci-inspector`
- Replace `utils/diff-full-config-in-env.sh` with new version of `utils/diff-full-config.sh`
- Docker: add entrypoint script to simplify the usage of utility scripts.
- Rename argument `--project` to `--gl-project` in `utils/run-pipeline.sh` for consistency

### New Features

- The `simulate pipeline` and `simulate job` commands now interpret
  `changes:` clauses of `rules:`. See `README.md`, section
  "Conditional jobs and includes" for details.
- The settings file can be specified with the `GCI_SETTINGS` environment variable.
- Settings can be set through environment variables, trumping settings loaded from file.
- Template hierarchy can be visualized with the 'visualize templates' command.
- The script `utils/diff-full-config.sh` can now compare one or
  multiple environments between two worktrees or arbitrary commits.
- Added the script `utils/merge-all-envs.sh` that merges all defined
  environments into separate files.
- Added the script `utils/run-pipeline.sh` spawning a pipeline with an
  arbitrary (or environment-defined) set of variables.
- Added the script `utils/log-full-config.sh`.

### Bug Fixes

- Fixed the 'lint' action to adapt to Gitlab API change.
- `utils/field-per-job.sh`: should now be possible to extract
  the field `stage` of each job.
- `run-pipeline.sh` is now a posix shell script and should be
  executable in the docker distributions.
- `run-pipeline.sh`: improve error message if an undefined environment
  is supplied.
