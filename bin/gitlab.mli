type config = {
  special_sections : (string * Yaml.value) list;
  templates : (string * Yaml.value) list;
  jobs : (string * Yaml.value) list;
}

val partition : (string * Yaml.value) list -> config

val departition : config -> (string * Yaml.value) list

type template_extensions_strategy = Expand | No_expansion | Debug_expansion

type inclusion_strategy =
  | Per_env of Conflang.Env.t
  | Empty_env
  | Always_include
  | Never_include

(** Raised when a local 'include:' can not be found in the file system *)
exception
  Inclusion_not_found of {
    path : string option;
        (** If [Some base], then [include_path] was included from [base].
       If [None], then [include_path] was include from the entrypoint
       file (typically [.gitlab-ci.yml]). *)
    include_path : string;  (** Path of the file that could not be found *)
  }

(** Raised if parsing an included file as a GitLab configuration fails *)
exception
  Parse_error of {
    path : string option;
        (** If [Some path], then the error arose happened when parsing
       [path]. If [None], then it arose in the entrypoint file
       (typically [.gitlab-ci.yml]). *)
    typ : string;  (** Type of configuration object parsed, e.g. 'rule'. *)
    message : string;
    value : Yaml.value;  (** The value *)
  }

(** Raised when attempting to include a missing template *)
exception
  Template_not_found of {
    path : string option;
    template_name : string;
    job_name : string;
    available_templates : string list;
    job_source : Yaml.value;
  }

(**
   Raises [Inclusion_not_found] if an inclusion can't be found.
   Raises [Parse_error] if an inclusion could not be parsed.
 *)
val merge :
  ?inclusion_strategy:inclusion_strategy ->
  ?apply_default:bool ->
  ?template_extensions_strategy:template_extensions_strategy ->
  ?exclude_templates:bool ->
  Fpath.t ->
  (string * Yaml.value) list ->
  (string * Yaml.value) list

val stage : string * Yaml.value -> string

val stages : config -> string list
