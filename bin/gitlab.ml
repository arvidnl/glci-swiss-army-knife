open Stuff
open Stuff.Util

exception Inclusion_not_found of {path : string option; include_path : string}

exception
  Parse_error of {
    path : string option;
    typ : string;
    message : string;
    value : Yaml.value;
  }

exception
  Template_not_found of {
    path : string option;
    template_name : string;
    job_name : string;
    available_templates : string list;
    job_source : Yaml.value;
  }

let special_sections_keys =
  ["image"; "stages"; "variables"; "include"; "workflow"; "default"]

let field_is_template key = '.' = key.[0]

let field_is_special_section key = List.mem key special_sections_keys

type config = {
  special_sections : (string * Yaml.value) list;
  templates : (string * Yaml.value) list;
  jobs : (string * Yaml.value) list;
}

let partition (ci_config : (string * Yaml.value) list) : config =
  let special_sections, templates, jobs =
    List.fold_left
      (fun (special_sections, templates, jobs) ((k, _) as kb) ->
        if field_is_special_section k then
          (kb :: special_sections, templates, jobs)
        else if field_is_template k then
          (special_sections, kb :: templates, jobs)
        else (special_sections, templates, kb :: jobs))
      ([], [], [])
      ci_config
  in
  {
    special_sections = List.rev special_sections;
    templates = List.rev templates;
    jobs = List.rev jobs;
  }

let departition {special_sections; templates; jobs} =
  special_sections @ templates @ jobs

type inclusion_strategy =
  | Per_env of Conflang.Env.t
  | Empty_env
  | Always_include
  | Never_include

let log_include ?include_base ?inclusion_strategy path do_include =
  Log.info
    "%s%s '%s' %s"
    (match include_base with
    | None -> "(top-level): "
    | Some path -> path ^ ": ")
    (if do_include then "Including" else "Excluding")
    path
    (match inclusion_strategy with
    | None -> "unconditionally"
    | Some strategy ->
        sf
          "conditionally according to strategy '%s'"
          (match strategy with
          | Always_include -> "always-include"
          | Never_include -> "never-include"
          | Empty_env -> "empty-env"
          | Per_env _env -> "env"))

type evaluate_include_rules_result =
  Conflang.Lang.when_val * Conflang.Lang.rule option

let evaluate_include_rules (env : Conflang.Env.t)
    (rules : Conflang.Lang.rule list) : evaluate_include_rules_result =
  Log.debug "Evaluating include rules:\n" ;
  List.iter (fun rule -> Log.debug " - %a\n" Conflang.Lang.pp_rule rule) rules ;
  let open Conflang.Lang in
  match rules with
  | [] -> (Always, None)
  | _ -> (
      (* See: https://docs.gitlab.com/ee/ci/yaml/includes.html#use-rules-with-include
         TODO: support [include:rules:changes:] *)
      match eval_rules ~changed_files:Base.String_set.empty env rules with
      | Some rule ->
          let when_ =
            match rule.when_ with
            | Some when_ ->
                (* todo add rule index *)
                Log.debug
                  "The rule `%a` matched, the rule-specific `when: %s` is  \
                   applied."
                  pp_rule
                  rule
                  (when_val_to_string when_) ;
                when_
            | None ->
                Log.debug
                  "The rule `%a` with no when matched so the file is included"
                  pp_rule
                  rule ;
                Always
          in
          (when_, Some rule)
      | None ->
          Log.debug
            "No rules matched, the file is not included in the configuration" ;
          (Never, None))

let rule_evaluator ?include_base path ?idx ~inclusion_strategy rules =
  (* https://docs.gitlab.com/ee/ci/yaml/includes.html#use-variables-with-include *)
  (* Are workflow variables defined at this point? I think that no. *)
  ignore idx ;
  let rules_parsed =
    match Conflang.Parse.workflow_rules rules with
    | Some (Some rules_parsed) -> rules_parsed
    | Some None | None ->
        raise
          (Parse_error
             {
               path = Some path;
               typ = "rule";
               message = "Could not parse rules.";
               value = rules;
             })
    | exception Conflang.Parse.Error {typ; message; value} ->
        raise (Parse_error {path = Some path; typ; message; value})
  in
  let evaluate env =
    match evaluate_include_rules env rules_parsed with
    | Always, _ -> true
    | Never, _ -> false
    | _ -> assert false
  in
  let do_include =
    match inclusion_strategy with
    | Always_include -> true
    | Never_include -> false
    | Empty_env -> evaluate (Conflang.Env.empty ())
    | Per_env env -> evaluate env
  in
  log_include ?include_base ~inclusion_strategy path do_include ;
  do_include

(** Finds and returns all includes, merged *)
let rec get_other_yaml_paths ?path ~inclusion_strategy base_dir
    (yaml_object : Yaml.value) : Yaml.value =
  let evaluate_include ?idx value =
    match value with
    | `String include_path ->
        log_include ?include_base:path include_path true ;
        Some include_path
    | `O _ -> (
        match YAML.(get "local" value, get "rules" value) with
        | `String include_path, `Null ->
            log_include ?include_base:path include_path true ;
            Some include_path
        | `String include_path, rules ->
            if
              rule_evaluator
                ?include_base:path
                include_path
                ?idx
                ~inclusion_strategy
                rules
            then Some include_path
            else None
        | _, _ ->
            failwith
              (sf
                 "Only support for 'local' includes is implemented%s, value: %s"
                 (Option.fold
                    ~none:""
                    ~some:(fun idx -> sf ", at index %d" idx)
                    idx)
                 (Yaml.to_string_exn value)))
    | _ ->
        failwith
          (sf
             "String or object expected in include%s, value: %s"
             (Option.fold
                ~none:""
                ~some:(fun idx -> sf ", at index %d" idx)
                idx)
             (Yaml.to_string_exn value))
  in
  let include_paths =
    match YAML.(yaml_object |-> "include") with
    | `A include_list ->
        List_util.filter_mapi
          (fun idx value -> evaluate_include ~idx value)
          include_list
    | (`String _ as include_value) | (`O _ as include_value) ->
        Option.to_list (evaluate_include include_value)
    | `Null -> []
    | _ as value ->
        failwith
          ("Unexpected type for the `include` key: expected array or string, \
            got " ^ YAML.tag value)
  in
  List.fold_left
    (fun yaml_object include_path ->
      let include_path_full = Fpath.(base_dir // v include_path) in
      if not (Sys.file_exists (Fpath.to_string include_path_full)) then
        raise (Inclusion_not_found {path; include_path}) ;
      match YAML.of_fpath_exn include_path_full with
      | `O _ as yaml_object' ->
          let yaml_object' : Yaml.value =
            get_other_yaml_paths
              ~path:include_path
              ~inclusion_strategy
              base_dir
              yaml_object'
          in
          YAML.merge_objects_append ~recursive:true [yaml_object; yaml_object']
      | _ ->
          failwith
          @@ Format.asprintf "Expected YAML object in file %s" include_path)
    yaml_object
    include_paths

(** Optionally returns the template this job extends *)
let get_extend_names (job_desc : (string * Yaml.value) list) =
  match List.assoc_opt "extends" job_desc with
  | None -> []
  | Some (`String s) -> [s]
  | Some (`A ss) -> List.map YAML.of_string_exn ss
  | _ ->
      failwith @@ "Expected string value reading `extends` field of: %a"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain (`O job_desc)

let _debug = true

(** Replaces the [extends: v] key-value pair, if present, into an obj
   with the key- values in [extend_content] *)
let replace_extend_by_definition ~debug_template_extension job_name job_desc
    extend_content =
  let desc_sans_extends = YAML.object_rem "extends" job_desc in
  let desc_sans_extends =
    if debug_template_extension then
      YAML.object_prepend_field
        "__gci_job_definition"
        (`String job_name)
        desc_sans_extends
    else desc_sans_extends
  in
  YAML.merge_objects_append ~recursive:true [extend_content; desc_sans_extends]

let _check_duplicates job_name obj =
  let keys = Hashtbl.create 10 in
  List.iter
    (fun (k, _) ->
      let count = succ @@ Option.value ~default:0 (Hashtbl.find_opt keys k) in
      Hashtbl.replace keys k count)
    (YAML.of_object_exn obj) ;
  let dups =
    Hashtbl.fold (fun k v dups -> if v > 1 then k :: dups else dups) keys []
  in
  if List.length dups > 0 then
    Format.eprintf
      "Warning: Template key overriding support is naive. Duplicate keys in \
       job `%s`: %s\n"
      job_name
      (String.concat ", " dups)

(** Replaces the [extends: v] key-value pair, if present, in the obj
   [job] using the values in [template_definitions]. solve_extensions
   is called recursively until all extends have been resolved. *)
let rec solve_extensions ~default ~debug_template_extension
    (recursion_depth : int) (template_definitions : (string * Yaml.value) list)
    (job_name, job_desc) : string * Yaml.value =
  let job_desc =
    YAML.of_object_exn
      ~err:"Only job as objects are supported for the moment"
      job_desc
  in
  let yaml_tag_object kind tag obj =
    let marker_prefix typ tag =
      String.make (recursion_depth + 2) '_'
      ^ "gci_" ^ kind ^ "_" ^ typ ^ "_" ^ tag ^ "_"
    in
    obj
    |> YAML.object_prepend_field (marker_prefix "begin" tag) (`String "begin")
    |> YAML.object_append_field (marker_prefix "__end" tag) (`String "end")
  in
  (* apply default: https://docs.gitlab.com/ee/ci/yaml/#default *)
  let job_desc =
    match default with
    | None -> job_desc
    | Some default ->
        YAML.of_object_exn
        @@ YAML.merge_objects_append
             ~recursive:true
             [
               (if debug_template_extension then
                yaml_tag_object "default" "" default
               else default);
               `O job_desc;
             ]
  in

  match get_extend_names job_desc with
  | [] -> (job_name, `O job_desc)
  | extend_names ->
      let extend_content : Yaml.value list =
        List.map
          (fun template_name ->
            match List.assoc_opt template_name template_definitions with
            | Some template_definition ->
                let _, expanded_template_definition =
                  solve_extensions
                  (* We only apply default on the outermost definition *)
                    ~default:None
                    ~debug_template_extension
                    (succ recursion_depth)
                    template_definitions
                    (template_name, template_definition)
                in
                if debug_template_extension then
                  yaml_tag_object
                    "extends"
                    template_name
                    expanded_template_definition
                else expanded_template_definition
            | None ->
                raise
                  (Template_not_found
                     {
                       path = None;
                       template_name;
                       job_name;
                       available_templates = List.map fst template_definitions;
                       job_source = `O job_desc;
                     }))
          extend_names
      in
      let extend_contents_merged =
        YAML.merge_objects_append ~recursive:true extend_content
      in
      (* check_duplicates job_name extend_contents_merged ; *)
      let expanded_job_desc =
        replace_extend_by_definition
          ~debug_template_extension
          job_name
          (`O job_desc)
          extend_contents_merged
      in
      (job_name, expanded_job_desc)

let solve_extensions ~default ~debug_template_extension template_definitions
    jobs =
  List.map
    (fun job ->
      if field_is_special_section (fst job) then job
      else
        solve_extensions
          ~default
          ~debug_template_extension
          0
          template_definitions
          job)
    jobs

let remove_include_section yaml_objects =
  let section_to_remove = ["include"] in
  List.filter (fun (k, _) -> not (List.mem k section_to_remove)) yaml_objects

type template_extensions_strategy = Expand | No_expansion | Debug_expansion

let merge ?(inclusion_strategy = Always_include) ?(apply_default = true)
    ?(template_extensions_strategy = No_expansion) ?(exclude_templates = false)
    base_dir yaml_object =
  let yaml_objects =
    get_other_yaml_paths ~inclusion_strategy base_dir (`O yaml_object)
    |> YAML.of_object_exn
  in
  let {templates; special_sections; _} = partition yaml_objects in
  let default =
    if apply_default then List.assoc_opt "default" special_sections else None
  in
  let yaml_objects = remove_include_section yaml_objects in
  let extended =
    match template_extensions_strategy with
    | Expand ->
        solve_extensions
          ~default
          ~debug_template_extension:false
          templates
          yaml_objects
    | Debug_expansion ->
        solve_extensions
          ~default
          ~debug_template_extension:true
          templates
          yaml_objects
    | No_expansion -> yaml_objects
  in
  if exclude_templates then
    List.filter (fun (field, _) -> not (field_is_template field)) extended
  else extended

let stages (config : config) =
  match List.assoc_opt "stages" config.special_sections with
  | Some (`A stages) -> List.map YAML.of_string_exn stages
  | _ ->
      (* If no stages are defined in .gitlab-ci.yml, then
         build, test and deploy are the default pipeline stages. *)
      Log.warn "Couldn't find stages, using the defaults" ;
      ["build"; "test"; "deploy"]

let stage (job : string * Yaml.value) =
  List.assoc_opt "stage" (YAML.of_object_exn (snd job))
  |> Option.map YAML.of_string_exn
  (* the default stage is "test" *)
  |> Option.value ~default:"test"
