(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Stuff
open Stuff.Base

type t = {name : string; environment : Conflang.Env.t; changes : String_set.t}

type map = (string, t) Hashtbl.t

let pp ?(verbose = true) fmt {name; environment; changes} =
  Format.fprintf fmt "Environment '%s'" name ;
  if verbose then (
    Format.fprintf fmt ":\n" ;
    Seq.iter
      (fun (var_name, var_value) ->
        Format.fprintf fmt " - %s: '%s'\n" var_name var_value)
      (Hashtbl.to_seq environment) ;
    if not (String_set.is_empty changes) then
      Format.fprintf fmt " - Changes: %a\n" String_set.pp changes)
  else Format.fprintf fmt ".\n"

let environment : t -> Conflang.Env.t =
 fun {name = _; environment; changes = _} -> environment

let changes : t -> String_set.t =
 fun {name = _; environment = _; changes} -> changes

let name {name; environment = _; changes = _} = name

let empty =
  {
    name = "[empty]";
    environment = Conflang.Env.empty ();
    changes = String_set.empty;
  }

let to_json : t -> Yojson.Basic.t =
 fun {environment; changes; name = _} ->
  let environment_json : (string * Yojson.Basic.t) list =
    List.map
      (fun (key, value) -> (key, `String value))
      (Conflang.Env.to_list environment)
  in
  let changes_json : (string * Yojson.Basic.t) list =
    if not (String_set.is_empty changes) then
      [
        ( "__changes",
          `List
            (changes |> String_set.to_seq
            |> Seq.map (fun s -> `String s)
            |> List.of_seq) );
      ]
    else []
  in
  `Assoc (environment_json @ changes_json)

let environments_to_settings_json : map -> Yojson.Basic.t =
 fun environments ->
  let environments_json =
    List.map
      (fun (env_name, env) -> (env_name, to_json env))
      (Hashtbl.to_seq environments |> List.of_seq)
  in
  `Assoc environments_json

let environments_of_settings_json_exn : Yojson.Basic.t -> map =
 fun json_settings ->
  JSON.of_assoc_exn json_settings
  |> List.map (fun (name, environment_json) ->
         let bindings = JSON.of_assoc_exn environment_json in
         let environment =
           List.filter (fun (key, _val) -> key <> "__changes") bindings
           |> List.map (fun (var_name, var_value) ->
                  (var_name, JSON.of_string_exn var_value))
           |> Conflang.Env.of_list
         in
         let changes =
           match List.assoc_opt "__changes" bindings with
           | Some changes_json ->
               changes_json |> Yojson.Basic.Util.to_list
               |> List.map Yojson.Basic.Util.to_string
               |> String_set.of_list
           | None -> String_set.empty
         in
         (name, {name; environment; changes}))
  |> List.to_seq |> Hashtbl.of_seq
