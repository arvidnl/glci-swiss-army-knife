(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Stuff.Base

(** A configuration environment represents a named run-time environment of
    a pipeline.

    Currently, the run-time environment includes:

    - a {!Conflang.Env.t} corresponding to the variables set in the pipeline.
    - a set of [changes], corresponding to the set of paths modified in the
      pipeline's branch. *)
type t

(** A name-indexed map of configuration environments *)
type map = (string, t) Hashtbl.t

(** Pretty-print an configuration environment.

    If [verbose] is true, also print the value of the environment's
    variables. *)
val pp : ?verbose:bool -> Format.formatter -> t -> unit

(** Retrieve the underlying !{Conflang.Env.t} *)
val environment : t -> Conflang.Env.t

(** [name config_env] is the name of [config_env] *)
val name : t -> string

(** The set of changes in the environment. *)
val changes : t -> String_set.t

(** An empty configuration environment named ["[empty]"].

    The underlying {!Conflang.Env.t} is empty. *)
val empty : t

(** Serialize a configuration environment to JSON *)
val to_json : t -> Yojson.Basic.t

(** Serialize a map of configuration environments to JSON *)
val environments_to_settings_json : map -> Yojson.Basic.t

(** Deserialize a map of configuration environments from JSON *)
val environments_of_settings_json_exn : Yojson.Basic.t -> map
