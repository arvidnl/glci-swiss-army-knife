(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2024 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Stuff
open Stuff.Base

type t = {
  base_dir : Fpath.t;
  ci_config : (string * Yaml.value) list;
  sort_keys : bool;
  settings : Settings.t;
}

let default : t =
  {
    base_dir = Fpath.v ".";
    ci_config = [];
    sort_keys = false;
    settings = Settings.default;
  }

let find_ci_config_file arg_ci_file_opt =
  let default = Fpath.v ".gitlab-ci.yml" in
  let file_exists p = Sys.file_exists (Fpath.to_string p) in
  let is_directory p = Sys.is_directory (Fpath.to_string p) in
  match arg_ci_file_opt with
  | None ->
      if file_exists default then default
      else
        error
          "Could not find an existing CI configuration file in the current \
           directory, pass '--ci-file'."
  | Some path when file_exists path ->
      if is_directory path then
        if file_exists Fpath.(path // default) then Fpath.(path // default)
        else
          error
            "Could not find an existing CI configuration file in directory: \
             '%a'"
            Fpath.pp
            path
      else path
  | Some path ->
      error "Not an existing CI configuration file: '%a'" Fpath.pp path

let setup
    ( arg_gitlab_api_config_opt,
      arg_ci_file_opt,
      arg_settings_file_path,
      arg_scalar_style_opt,
      arg_sort_keys,
      arg_log_level_debug,
      arg_log_level_info,
      arg_gitlab_project ) =
  (* Set log level *)
  (match (arg_log_level_debug, arg_log_level_info) with
  | true, _ -> Log.(current_log_level := Debug)
  | _, true -> Log.(current_log_level := Info)
  | false, false -> Log.(current_log_level := Report)) ;
  let settings =
    let settings = Settings.find arg_settings_file_path in
    {
      settings with
      scalar_style =
        Option.value arg_scalar_style_opt ~default:settings.scalar_style;
      gitlab_token =
        Option.value arg_gitlab_api_config_opt ~default:settings.gitlab_token;
      gitlab_project =
        Option_util.join arg_gitlab_project settings.gitlab_project;
    }
  in
  let arg_ci_file = find_ci_config_file arg_ci_file_opt in
  let base_dir = fst (Fpath.split_base arg_ci_file) in
  let (ci_config : Yaml.value) = YAML.of_fpath_exn arg_ci_file in
  match ci_config with
  | `O ci_config -> {ci_config; base_dir; sort_keys = arg_sort_keys; settings}
  | _ ->
      error
        "Invalid GitLab CI file %a, it should contain a YAML object."
        Fpath.pp
        arg_ci_file
