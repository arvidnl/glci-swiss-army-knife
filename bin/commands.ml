(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2024 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Stuff
open Stuff.Base

type inclusion_strategy_named =
  | Per_named_env of Config_environment.t
  | Empty_env
  | Always_include
  | Never_include

let inclusion_strategy_named_to_string = function
  | Per_named_env config_env -> "env:" ^ Config_environment.name config_env
  | Empty_env -> "empty-env"
  | Always_include -> "always-include"
  | Never_include -> "never-include"

let unname_inclusion_strategy_named = function
  | Per_named_env config_env ->
      Gitlab.Per_env (Config_environment.environment config_env)
  | Empty_env -> Gitlab.Empty_env
  | Always_include -> Gitlab.Always_include
  | Never_include -> Gitlab.Never_include

let extension_strategies =
  let open Gitlab in
  [
    ("no-expansion", No_expansion);
    ("debug-expansion", Debug_expansion);
    ("expand", Expand);
  ]

let evaluation_strategies =
  [
    ("always-include", Always_include);
    ("never-include", Never_include);
    ("empty-env", Empty_env);
  ]

let yaml_output ~sort_keys ~scalar_style obj =
  let obj = if sort_keys then YAML.sort_keys_recursively obj else obj in
  print_string (Yaml.to_string_exn ~scalar_style obj)

(** {2 Focus} *)
module Focus = struct
  let focus (no_preamble, no_needs, no_retry, template_extensions_strategy)
      (focused_job_name, focused_job)
      Context.{ci_config; base_dir; settings = {scalar_style; _}; sort_keys; _}
      =
    let expanded_yaml_objects =
      Gitlab.merge ~template_extensions_strategy base_dir ci_config
    in
    let Gitlab.{special_sections; _} = Gitlab.partition expanded_yaml_objects in
    let filtered_job = focused_job template_extensions_strategy in
    (* List.assoc focused_job expanded_yaml_objects in *)
    let preamble = if no_preamble then [] else special_sections in
    let filtered_job =
      `O
        YAML.(
          filtered_job |> of_object_exn
          |> List.filter (function
                 | ("needs", _ | "dependencies", _) when no_needs -> false
                 | "retry", _ when no_retry -> false
                 | _ -> true))
    in
    let final_content = preamble @ [(focused_job_name, filtered_job)] in
    yaml_output ~sort_keys ~scalar_style (`O final_content)
end

(** {2 Linting} *)
module Lint = struct
  let format_results ~include_merged_yaml
      (linting_response_err :
        (Gitlab_api.linting_response, Gitlab_api.error) Result.t) =
    match linting_response_err with
    | Ok {valid; merged_yaml; errors; warnings} ->
        Format.printf " %s!\n" (if valid then "valid" else "invalid") ;
        (match (include_merged_yaml, merged_yaml) with
        | true, Some merged_yaml ->
            Format.printf "Merged YAML:\n%s\n" merged_yaml
        | true, None -> Format.printf "No merged YAML in response\n"
        | false, _ -> ()) ;
        if not valid then (
          Format.printf "Errors:\n" ;
          List.iter (fun e -> Format.printf " - %s\n" e) errors) ;
        if List.length warnings > 0 then (
          Format.printf "Warnings:\n" ;
          List.iter (fun e -> Format.printf " - %s\n" e) warnings)
    | Error Gitlab_api.Unauthorized ->
        error
          "Received response '401 Unauthorized' from GitLab API: Is your \
           gitlab access token valid?"
    | Error (Gitlab_api.Other (msg, json)) ->
        error
          "Received message `%s`: %a"
          msg
          (Yojson.Basic.pretty_print ~std:true)
          json
    | Error (Gitlab_api.Unrecognized_json json) ->
        error
          "Could not parse successful JSON result: `%a`"
          (Yojson.Basic.pretty_print ~std:true)
          json
    | Error (Gitlab_api.Unknown json) ->
        error
          "not field `status` nor `message` field in `%a`"
          (Yojson.Basic.pretty_print ~std:true)
          json

  let lint : bool * inclusion_strategy_named -> Context.t -> unit Lwt.t =
   fun (include_merged_yaml, evaluation_strategy)
       {ci_config; base_dir; settings; _} ->
    let bin = Sys.argv.(0) in
    let token, project =
      match (settings.gitlab_token, settings.gitlab_project) with
      | None, _ ->
          error
            "The 'lint' action requires you to set a GitLab Access token \
             (https://gitlab.com/-/profile/personal_access_tokens) either \
             through '--gl-access-token' or in a '.gci.json' file (see '%s \
             man' for more on '.gci.json')."
            bin
      | _, None ->
          error
            "The 'lint' action requires you to specify a GitLab project \
             towards whose API the lint query is executed. This is set either \
             through '--gl-project' or in a '.gci.json' file (see '%s man' for \
             more on '.gci.json')."
            bin
      | Some token, Some project -> (token, project)
    in
    let ci_config =
      Gitlab.merge
        ~inclusion_strategy:
          (unname_inclusion_strategy_named evaluation_strategy)
        base_dir
        ci_config
    in
    let ci_config =
      Yaml.to_string_exn ~scalar_style:settings.scalar_style (`O ci_config)
    in
    Format.printf
      "Linting [strategy: %s] (this might take a while on large \
       configurations)..."
      (inclusion_strategy_named_to_string evaluation_strategy) ;
    Format.print_flush () ;
    let open Lwt.Syntax in
    let* linting_response =
      Gitlab_api.lint ~token ~project ~content:ci_config ()
    in
    format_results ~include_merged_yaml linting_response ;
    match linting_response with
    | Ok {valid = true; _} -> Lwt.return_unit
    | _ -> exit 1
end

(** {2 Simulate} *)
module Simulate = struct
  let cutoff placeholder n l =
    let rec sublist_opt b e l =
      match l with
      | [] -> None
      | h :: t ->
          let tail = if e = 0 then Some [] else sublist_opt (b - 1) (e - 1) t in
          if b > 0 then tail else Option.map (List.cons h) tail
    in
    let len = List.length l in
    if len <= abs n then l
    else if n = 0 then [placeholder]
    else if n >= 0 then Option.get (sublist_opt 0 (n - 1) l) @ [placeholder]
    else placeholder :: Option.get (sublist_opt (len + n) (len - 1) l)

  let bin : ('v -> 'k) -> 'v list -> ('k, 'v list) Hashtbl.t =
   fun f_label l ->
    List.fold_right
      (fun el bins ->
        let lbl = f_label el in
        let bin = Hashtbl.find_opt bins lbl |> Option.value ~default:[] in
        Hashtbl.replace bins lbl (el :: bin) ;
        bins)
      l
      (Hashtbl.create 5)

  let pp_job_tree ~include_empty_stages ?cutoff_opt fmt (stages, jobs) =
    let when_to_string w =
      let open Conflang.Lang in
      let color =
        match w with
        | On_success -> Color.(FG.green)
        | On_failure -> Color.(FG.magenta)
        | Always -> Color.(bold ++ FG.green)
        | Manual -> Color.(FG.gray)
        | Delayed -> Color.(FG.blue)
        | Never -> Color.(FG.red)
      in
      Color.apply
        color
        (Format.asprintf "[%s]" (Conflang.Lang.when_val_to_string w))
    in
    let jobs_by_stage = bin (fun (job, _) -> Gitlab.stage job) jobs in
    let leaves =
      let open Tree in
      List.filter_map
        (fun stage ->
          let jobs =
            Hashtbl.find_opt jobs_by_stage stage |> Option.value ~default:[]
          in
          if List.length jobs = 0 && not include_empty_stages then None
          else
            let leafs =
              List.map
                (fun ((job_name, _), (when_, variables)) ->
                  let label =
                    Format.(
                      asprintf
                        "%s%s%s"
                        (Option.fold
                           ~none:""
                           ~some:(fun when_ ->
                             asprintf "%s " (when_to_string when_))
                           when_)
                        job_name
                        (Option.fold
                           ~none:""
                           ~some:(fun vars ->
                             if Conflang.Env.is_empty vars then ""
                             else
                               Color.(
                                 apply FG.gray
                                 @@ asprintf " %a" Conflang.Env.pp vars))
                           variables))
                  in
                  Leaf label)
                (List.sort
                   (fun ((job_name1, _), _) ((job_name2, _), _) ->
                     String.compare job_name1 job_name2)
                   jobs)
            in
            let leafs =
              match cutoff_opt with
              | None -> leafs
              | Some n -> cutoff (Leaf "<...>") n leafs
            in
            Some (Node (stage, leafs)))
        stages
    in
    let tree =
      if List.length leaves = 0 then Tree.Node ("CI Pipeline (empty)", [])
      else Tree.Node ("CI Pipeline", leaves)
    in
    let pp_stage fmt s = Format.fprintf fmt "[%s]" Color.(apply bold s) in
    let pp_job fmt s = Format.fprintf fmt " %s" s in
    Format.fprintf fmt "%a" (Tree.pp_fancy pp_job pp_stage) tree

  type evaluate_job_rules_result =
    Conflang.Lang.when_val * Conflang.Lang.rule option * Conflang.Env.t

  let evaluate_job_rules (env : Conflang.Env.t) ~changes ~job_name
      (job : Conflang.Lang.job) : evaluate_job_rules_result =
    Log.debug "Evaluating rules:\n" ;
    List.iter
      (fun rule -> Log.debug " - %a\n" Conflang.Lang.pp_rule rule)
      (Option.value ~default:[] job.rules) ;

    (* If a rule matches and has no [when] defined, the rule uses the [when]
       defined for the job, which defaults to [on_success] if not defined.

       https://docs.gitlab.com/ee/ci/yaml/#rulesif
    *)
    (* let default_when = job.when_ in *)
    let open Conflang.Lang in
    match job.rules with
    | Some rules -> (
        match eval_rules ~job_name ~changed_files:changes env rules with
        | Some rule ->
            let when_ =
              match (rule.when_, job.when_) with
              | Some when_, _ ->
                  (* todo add rule index *)
                  Log.debug
                    "The rule `%a` matched, the rule-specific `when: %s` is  \
                     applied."
                    pp_rule
                    rule
                    (when_val_to_string when_) ;
                  when_
              | None, Some when_ ->
                  Log.debug
                    "The rule `%a` matched, the job-specific `when: %s` is \
                     applied."
                    pp_rule
                    rule
                    (when_val_to_string when_) ;
                  when_
              | None, None ->
                  Log.debug
                    "The rule `%a` matched. The rule nor the job has a when, \
                     the default `when: %s` is applied."
                    pp_rule
                    rule
                    (when_val_to_string job_default_when) ;
                  job_default_when
            in
            (when_, Some rule, rule.variables)
        | None ->
            Log.debug "No rules matched, the job is not added to the pipeline" ;
            (Never, None, Conflang.Env.empty ()))
    | None ->
        let when_ =
          match job.when_ with
          | Some when_ ->
              Log.debug
                "The job has no rules, the job's `when: %s` is applied."
                (when_val_to_string when_) ;
              when_
          | None ->
              Log.debug
                "The job has no rules nor any when, the default `when: %s` is \
                 applied."
                (when_val_to_string job_default_when) ;
              job_default_when
        in
        (when_, None, Conflang.Env.empty ())

  type evaluate_workflow_rules_result =
    Conflang.Lang.workflow_when_val * Conflang.Lang.rule option * Conflang.Env.t

  let evaluate_workflow_rules ci_config_partitioned (env : Conflang.Env.t)
      ~changes : evaluate_workflow_rules_result =
    let Gitlab.{special_sections; _} = ci_config_partitioned in
    match List.assoc_opt "workflow" special_sections with
    | Some workflow -> (
        Log.debug "Workflow: %a\n" Yaml.pp workflow ;
        let workflow =
          match Conflang.Parse.workflow workflow with
          | None -> error "Could not parse the workflow section."
          | Some workflow -> workflow
        in
        Log.debug "Evaluating rules:\n" ;
        List.iter
          (fun rule -> Log.debug " - %a\n" Conflang.Lang.pp_rule rule)
          workflow.rules ;
        let rule_opt =
          Conflang.Lang.eval_rules ~changed_files:changes env workflow.rules
        in
        Option.iter
          (fun (rule : Conflang.Lang.rule) ->
            Conflang.Env.(merge (empty ()) rule.variables))
          rule_opt ;
        match rule_opt with
        (* https://docs.gitlab.com/ee/ci/yaml/#workflowrules

           implictly, the absence of a when in a workflow rule means Always
        *)
        | Some {when_ = None | Some Always; variables; _} ->
            (Conflang.Lang.Always, rule_opt, variables)
        | Some {when_ = Some Never; variables; _} ->
            (Conflang.Lang.Never, rule_opt, variables)
        (* https://docs.gitlab.com/ee/ci/yaml/#workflowrules:
         * If no rule matched, then do not create pipeline *)
        | None -> (Conflang.Lang.Never, None, Conflang.Env.empty ())
        (* [when] other than [Always] and [Never] are not allowed for workflow rules *)
        | Some {when_ = _; _} -> error "impossible")
    | None ->
        (* If there is no workflow section, then we always allow the pipeline *)
        Log.info "No workflow detected" ;
        (Always, None, env)

  let on_pipeline_creation env_variables (v : evaluate_workflow_rules_result)
      ~on_no_creation on_creation =
    match v with
    | Always, rule_opt, variables ->
        (match rule_opt with
        | Some rule ->
            Log.info
              "Pipeline created by workflow rule: %a"
              Conflang.Lang.pp_rule
              rule
        | None -> Log.info "A pipeline was created as no rule blocked it.") ;
        Conflang.Env.merge env_variables variables ;
        on_creation (rule_opt, variables)
    | Never, None, _variables ->
        Log.info "No pipeline would be created as no workflow rule matched." ;
        on_no_creation None
    | Never, Some rule, _variables ->
        Log.report
          "No pipeline would be created, blocked by workflow rule: %a"
          Conflang.Lang.pp_rule
          rule ;
        on_no_creation (Some rule)

  let load_initial_environment :
      Config_environment.t -> Gitlab.config -> Conflang.Env.t =
   fun config_env ci_config_partitioned ->
    let initial_environment = Conflang.Env.empty () in
    let env_variables = Config_environment.environment config_env in
    let Gitlab.{special_sections; _} = ci_config_partitioned in
    let ci_config_variables =
      match List.assoc_opt "variables" special_sections with
      | Some v -> (
          match Conflang.Parse.env v with
          | Some env -> env
          | None ->
              error
                "Could not load global environment: `%s`."
                (Yaml.to_string_exn v))
      | None -> Conflang.Env.empty ()
    in
    Hashtbl.iter
      (fun var_name env_var_value ->
        match Hashtbl.find_opt ci_config_variables var_name with
        | Some ci_config_value ->
            Log.warn
              "The CI configurations global variable {%s=%s} was overriden by \
               the environment's value `%s`"
              var_name
              ci_config_value
              env_var_value
        | None -> ())
      env_variables ;
    Conflang.Env.merge initial_environment ci_config_variables ;
    Conflang.Env.merge initial_environment env_variables ;
    Log.debug
      "Loaded initial environment: %a"
      Conflang.Env.pp
      initial_environment ;
    initial_environment

  let simulate_job_aux ?(log_level = Log.Info) ~changes env_variables job_name
      job =
    let job_parsed =
      match Conflang.Parse.job job with
      | None -> error "Could not parse job `%s`." job_name
      | Some job -> job
    in
    let when_, _opt_rule, _variables =
      evaluate_job_rules env_variables ~job_name job_parsed ~changes
    in
    match when_ with
    | Conflang.Lang.Never ->
        Log.log
          ~level:log_level
          "Job %s is not triggered by rule %a"
          job_name
          (Format.pp_print_option
             ~none:(fun fmt () -> Format.fprintf fmt "<none>")
             Conflang.Lang.pp_rule)
          _opt_rule ;
        None
    | _ ->
        Log.log
          ~level:log_level
          "Job '%s' is triggered '%a' by rule %a"
          job_name
          Conflang.Lang.pp_when_val
          when_
          (Format.pp_print_option
             ~none:(fun fmt () -> Format.fprintf fmt "<none>")
             Conflang.Lang.pp_rule)
          _opt_rule ;
        Some ((job_name, job), (Some when_, Some _variables))

  let simulate_job (job_name, job) conf_env Context.{ci_config; base_dir; _} =
    Log.info
      "Simulating the job '%s' in environment '%s'."
      job_name
      (Config_environment.name conf_env) ;
    Log.debug "%a" (Config_environment.pp ~verbose:true) conf_env ;
    let changes = Config_environment.changes conf_env in
    let ci_config_merged =
      Gitlab.merge
        ~inclusion_strategy:
          (Gitlab.Per_env (Config_environment.environment conf_env))
        ~template_extensions_strategy:Expand
        base_dir
        ci_config
    in
    let ci_config_partitioned = Gitlab.partition ci_config_merged in
    let env_variables =
      load_initial_environment conf_env ci_config_partitioned
    in
    on_pipeline_creation
      env_variables
      (evaluate_workflow_rules ci_config_partitioned env_variables ~changes)
      ~on_no_creation:(fun _ -> ())
    @@ fun (_workflow_rule_opt, _updated_variables) ->
    let job = job Gitlab.Expand in
    ignore
      (simulate_job_aux
         ~log_level:Log.Report
         ~changes
         env_variables
         job_name
         job)

  let simulate_pipeline : Config_environment.t -> Context.t -> unit =
   fun conf_env {ci_config; base_dir; _} ->
    Log.info
      "Simulating pipeline using environment `%s`:"
      (Config_environment.name conf_env) ;
    Log.debug "%a" (Config_environment.pp ~verbose:true) conf_env ;
    let changes = Config_environment.changes conf_env in
    let ci_config_merged =
      Gitlab.merge
        ~inclusion_strategy:
          (Gitlab.Per_env (Config_environment.environment conf_env))
        ~template_extensions_strategy:Expand
        base_dir
        ci_config
    in
    let ci_config_partitioned = Gitlab.partition ci_config_merged in
    let env_variables =
      load_initial_environment conf_env ci_config_partitioned
    in
    on_pipeline_creation
      env_variables
      (evaluate_workflow_rules ci_config_partitioned env_variables ~changes)
      ~on_no_creation:(fun _ -> ())
    @@ fun (_workflow_rule_opt, _updated_variables) ->
    let jobs =
      List.filter_map
        (fun (job_name, job) ->
          simulate_job_aux ~changes env_variables job_name job)
        ci_config_partitioned.jobs
    in
    Format.printf
      "%a"
      (pp_job_tree ~include_empty_stages:true ?cutoff_opt:None)
      (Gitlab.stages ci_config_partitioned, jobs)

  let visualize_pipeline : int option -> Context.t -> unit =
   fun cutoff_opt {ci_config; base_dir; _} ->
    let ci_config = Gitlab.merge base_dir ci_config in
    let ci_config_partioned = Gitlab.partition ci_config in
    Format.printf
      "%a"
      (pp_job_tree ~include_empty_stages:true ?cutoff_opt)
      ( Gitlab.stages ci_config_partioned,
        List.map (fun job -> (job, (None, None))) ci_config_partioned.jobs )

  let merge
      ( template_extensions_strategy,
        evaluation_strategy,
        exclude_templates,
        exclude_untriggered,
        strip_keys )
      Context.{ci_config; base_dir; settings = {scalar_style; _}; sort_keys; _}
      =
    let ci_config_merged =
      Gitlab.merge
        ~exclude_templates
        ~inclusion_strategy:
          (unname_inclusion_strategy_named evaluation_strategy)
        ~template_extensions_strategy
        base_dir
        ci_config
    in
    let ci_config_partitioned = Gitlab.partition ci_config_merged in
    let ci_config_partitioned =
      if not exclude_untriggered then ci_config_partitioned
      else
        let environment =
          match evaluation_strategy with
          | Per_named_env v -> v
          | Empty_env -> Config_environment.empty
          | Always_include | Never_include ->
              error
                "The option '--exclude-untriggered-jobs' is incompatible with \
                 '--evaluation-strategy always-include' and \
                 '--evaluation-strategy never-include'. Please either set \
                 '--evaluation-strategy <ENV>' or '--evaluation-strategy \
                 empty-env'"
        in
        let env_variables =
          load_initial_environment environment ci_config_partitioned
        in
        let jobs =
          let changes = Config_environment.changes environment in
          on_pipeline_creation
            env_variables
            (evaluate_workflow_rules
               ci_config_partitioned
               env_variables
               ~changes)
            ~on_no_creation:(fun _ -> [])
          @@ fun _ ->
          let triggered_jobs =
            List.filter
              (fun (job_name, job) ->
                Option.is_some
                @@ simulate_job_aux ~changes env_variables job_name job)
              ci_config_partitioned.jobs
          in
          triggered_jobs
        in
        let ci_config_partitioned = {ci_config_partitioned with jobs} in
        ci_config_partitioned
    in
    let ci_config_partitioned =
      match strip_keys with
      | [] -> ci_config_partitioned
      | keys_to_remove ->
          let jobs =
            List.map
              (fun (job_name, job) ->
                ( job_name,
                  YAML.object_filter_keys job @@ fun (key, _value) ->
                  not (List.mem key keys_to_remove) ))
              ci_config_partitioned.jobs
          in
          {ci_config_partitioned with jobs}
    in
    let output_config = `O (Gitlab.departition ci_config_partitioned) in
    yaml_output ~sort_keys ~scalar_style output_config
end

(** {2 Enviroments } *)
module Environments = struct
  type environment_format = Terse | Verbose | Json

  let environment_format_of_string = function
    | "json" -> Some Json
    | "terse" -> Some Terse
    | "verbose" -> Some Verbose
    | _ -> None

  let show_environment format env =
    match format with
    | Terse | Verbose ->
        Format.printf
          "%a"
          (* TODO: terse doens't really make any sense here *)
          (Config_environment.pp ~verbose:true)
          env
    | Json ->
        let json = Config_environment.to_json env in
        Format.printf "%a\n" (Yojson.Basic.pretty_print ~std:true) json

  let show_environments format Context.{settings; _} =
    match format with
    | Terse | Verbose ->
        Format.printf "Available environments:\n" ;
        List.iter
          (fun env ->
            Format.printf
              "%a"
              (Config_environment.pp ~verbose:(format = Verbose))
              env)
          (Hashtbl.to_seq_values settings.environments |> List.of_seq)
    | Json ->
        let json =
          `Assoc
            [
              ( "environments",
                Config_environment.environments_to_settings_json
                  settings.environments );
            ]
        in
        Format.printf "%a\n" (Yojson.Basic.pretty_print ~std:true) json
end

(** {2 Shellcheck} *)
module Shellcheck = struct
  let variables_of_definition def =
    let open Result_util.Syntax in
    match List.assoc_opt "variables" def with
    | None -> Ok []
    | Some vs ->
        Result_util.list_map_e
          (fun (var, value) ->
            let+ value =
              match value with
              | `Bool true -> Ok {|"true"|}
              | `Bool false -> Ok {|"false"|}
              | `Null -> Ok {|""|}
              | `Float f -> Ok (string_of_float f)
              | `String s -> Ok ("\"" ^ s ^ "\"")
              | `O _ ->
                  fail
                    (Format.asprintf
                       "Unexpected type object in variable '%s'"
                       var)
              | `A _ ->
                  fail
                    (Format.asprintf
                       "Unexpected type array in variable '%s'"
                       var)
            in
            (var, value))
          (YAML.of_object_exn vs)

  let variables_to_script ?(disable_unused_shellcheck_warning = false) header
      vars =
    (("# " ^ header)
     :: List.map
          (fun (var, value) ->
            (if disable_unused_shellcheck_warning then
             "#shellcheck disable=SC2034\n"
            else "")
            ^ var ^ "=" ^ value)
          vars
    |> String.concat "\n")
    ^ "\n"

  let full_script_of_job ?(environment : Config_environment.t option)
      special_sections (job_definition : Yaml.value) : (string, string) result =
    let shellcheck_preamble =
      {|#shellcheck enable=check-unassigned-uppercase

# dummy command as described here
# https://github.com/koalaman/shellcheck/issues/960#issuecomment-318918175
true
|}
    in
    let script_preamble =
      String.concat
        "\n"
        (List.concat_map
           (fun ci_environment_var ->
             ["#shellcheck disable=SC2034"; ci_environment_var ^ {|=""|}])
           Gitlab_ci_environment_variables.all)
    in
    let open Result_util.Syntax in
    let* global_variables = variables_of_definition special_sections in
    let* job_variables =
      variables_of_definition (YAML.of_object_exn job_definition)
    in
    let script =
      (YAML.(job_definition |-> "before_script" |> of_string_or_array)
      @ YAML.(job_definition |-> "script" |> of_string_or_array)
      @ YAML.(job_definition |-> "after_script" |> of_string_or_array))
      |> String.concat "\n"
    in
    return
    @@ String.concat
         "\n"
         ([
            "#!/bin/bash\n";
            shellcheck_preamble;
            script_preamble;
            variables_to_script
              ~disable_unused_shellcheck_warning:true
              "Global variables"
              global_variables;
          ]
         @ Option.fold
             ~none:[]
             ~some:(fun config_env ->
               [
                 variables_to_script
                   ~disable_unused_shellcheck_warning:true
                   (Format.asprintf
                      "Environment variables [%s]"
                      (Config_environment.name config_env))
                   (config_env |> Config_environment.environment
                  |> Conflang.Env.to_list);
               ])
             environment
         @ [
             "\n";
             variables_to_script "Job variables" job_variables;
             "\n";
             "# Job script: \n";
             script;
           ])

  let with_tempfile ?temp_dir ~prefix ~suffix f =
    let tempfile = Filename.temp_file ?temp_dir prefix suffix in
    Fun.protect ~finally:(fun () -> Sys.remove tempfile) (fun () -> f tempfile)

  let full_script_of_job_in_tmp_file ?environment special_sections job
      (job_definition : Yaml.value) f =
    let open Lwt.Syntax in
    match full_script_of_job ?environment special_sections job_definition with
    | Error e -> Lwt.fail_with e
    | Ok script ->
        Lwt_io.with_temp_dir ~prefix:"gci" @@ fun tmpdir ->
        let script_filename = tmpdir ^ "/" ^ job ^ ".sh" in
        let* () =
          Lwt_io.with_file ~mode:Lwt_io.output script_filename (fun file ->
              Lwt_io.write file script)
        in
        f script_filename

  let shellcheck_job base_dir special_sections (job : string)
      (job_definition : Yaml.value) =
    let open Lwt.Syntax in
    full_script_of_job_in_tmp_file special_sections job job_definition
    @@ fun script_filename ->
    let* res =
      Lwt_process.exec
        ~cwd:(Fpath.to_string base_dir)
        ("", [|"shellcheck"; "-x"; script_filename|])
    in
    match res with Unix.WEXITED 0 -> Lwt.return true | _ -> Lwt.return false

  let one_job :
      string * (Gitlab.template_extensions_strategy -> Yaml.value) ->
      Context.t ->
      unit Lwt.t =
   fun (job_name, job) {ci_config; base_dir; _} ->
    let open Lwt.Syntax in
    let expanded_yaml_objects =
      Gitlab.merge
        ~template_extensions_strategy:Gitlab.Expand
        base_dir
        ci_config
    in
    let Gitlab.{special_sections; _} = Gitlab.partition expanded_yaml_objects in
    let focused_job = job Gitlab.Expand in
    let* () = Lwt_io.printf "Shellchecking job `%s`: " job_name in
    let* shellchecked =
      shellcheck_job base_dir special_sections job_name focused_job
    in
    if shellchecked then
      let* () = Lwt_io.printf "OK.\n" in
      Lwt.return_unit
    else error "Job `%s` failed shellcheck." job_name

  let all_jobs : Context.t -> unit Lwt.t =
   fun {ci_config; base_dir; _} ->
    let expanded_yaml_objects =
      Gitlab.merge
        ~template_extensions_strategy:Gitlab.Expand
        base_dir
        ci_config
    in
    let Gitlab.{special_sections; jobs; _} =
      Gitlab.partition expanded_yaml_objects
    in
    let open Lwt.Syntax in
    let rec loop shellcheck_failures = function
      | (job, job_definition) :: jobs ->
          let* () = Lwt_io.printf "Shellchecking job `%s`.\n" job in
          let* result =
            shellcheck_job base_dir special_sections job job_definition
          in
          if result then loop shellcheck_failures jobs
          else loop (job :: shellcheck_failures) jobs
      | [] -> Lwt.return (List.rev shellcheck_failures)
    in
    let* shellcheck_failures = loop [] jobs in
    match shellcheck_failures with
    | [] ->
        let* () = Lwt_io.printf "All jobs OK!\n" in
        Lwt.return_unit
    | failed_jobs ->
        error
          "Following jobs failed shellcheck:\n@[<v>%a@]"
          (Format.pp_print_list
             ~pp_sep:Format.pp_print_newline
             Format.pp_print_string)
          failed_jobs

  let exec_one_job :
      Config_environment.t option -> string -> Context.t -> unit Lwt.t =
   fun environment focused_job {ci_config; base_dir; _} ->
    let open Lwt.Syntax in
    let expanded_yaml_objects =
      Gitlab.merge ~template_extensions_strategy:Expand base_dir ci_config
    in
    let Gitlab.{special_sections; jobs; _} =
      Gitlab.partition expanded_yaml_objects
    in
    let filtered_job =
      match List.assoc_opt focused_job expanded_yaml_objects with
      | None ->
          error
            "No job `%s`. Available jobs:\n@[<v>%a@]"
            focused_job
            (Format.pp_print_list
               ~pp_sep:Format.pp_print_newline
               Format.pp_print_string)
            (List.map fst jobs)
      | Some filtered_job -> filtered_job
    in
    let image = YAML.(filtered_job |-> "image" |> of_string_exn) in
    let* () =
      Lwt_io.printf
        "Executing job '%s' using image '%s'%s:\n"
        focused_job
        image
        (Option.fold
           ~none:""
           ~some:(fun config_env ->
             Format.asprintf
               " in environment '%s'"
               (Config_environment.name config_env))
           environment)
    in
    let* execution_result =
      full_script_of_job_in_tmp_file
        ?environment
        special_sections
        focused_job
        filtered_job
      @@ fun script_filename ->
      let* fd = Lwt_unix.openfile script_filename [Unix.O_RDONLY] 0o666 in
      let cwd = Fpath.to_string base_dir in
      let pwd = Fpath.to_string base_dir in
      Lwt_process.exec
        ~cwd
        ~stdin:(`FD_move (Lwt_unix.unix_file_descr fd))
        ( "",
          [|
            "docker";
            "run";
            "--volume";
            Format.asprintf "%s:%s" pwd pwd;
            "-w";
            pwd;
            "--interactive";
            "--entrypoint";
            "/bin/sh";
            image;
          |] )
    in
    match execution_result with
    | Unix.WEXITED 0 ->
        let* () = Lwt_io.printf "Execution OK.\n" in
        Lwt.return_unit
    | _ -> error "Job `%s` failed execution." focused_job
end

(** {2 Visualize} *)
module Visualize = struct
  (* A template tree has leafs corresponding to job names, and
     internal nodes are tuples [job_name, children_total] where
     [children_total] is the size of the subtree starting at the node,
     including the node it self. *)
  type template_tree = (string, string * int) Tree.tree

  let visualize_templates : bool * bool -> string list -> Context.t -> unit =
   fun (reverse, no_prune) roots {ci_config; base_dir; _} ->
    let ci_config =
      Gitlab.merge ~template_extensions_strategy:No_expansion base_dir ci_config
    in
    let ci_config_partioned = Gitlab.partition ci_config in
    (* Maps job name to its path *)
    let job_extends_map : (string, string list) Hashtbl.t =
      let tbl =
        ci_config_partioned.templates @ ci_config_partioned.jobs
        |> List.map (fun (job_name, _value) -> (job_name, []))
        |> List.to_seq |> Hashtbl.of_seq
      in
      List.iter
        (fun (job_name, value) ->
          let job_extends =
            match Yaml.Util.find "extends" value with
            | Error _ | Ok None -> []
            | Ok (Some job_templates) -> (
                match job_templates with
                | `A job_templates ->
                    List.map Yaml.Util.to_string_exn job_templates
                | `String s -> [s]
                | _ -> [])
          in
          let job_extends =
            List.filter
              (fun template ->
                match Hashtbl.find_opt tbl template with
                | Some _ -> true
                | None ->
                    Log.warn
                      "Job '%s' extends '%s', but no such job or template is \
                       defined -- it will be ignored."
                      job_name
                      template ;
                    false)
              job_extends
          in
          Hashtbl.replace tbl job_name job_extends)
        (ci_config_partioned.templates @ ci_config_partioned.jobs) ;
      tbl
    in
    (* Maps job name to jobs that extend it *)
    let job_extendee_map : (string, string list) Hashtbl.t =
      let tbl = Hashtbl.create 5 in
      Hashtbl.iter
        (fun job_name job_extends ->
          (match Hashtbl.find_opt tbl job_name with
          | None -> Hashtbl.add tbl job_name []
          | Some _ -> ()) ;
          List.iter
            (fun template ->
              let extensions =
                Option.value ~default:[] @@ Hashtbl.find_opt tbl template
              in
              Hashtbl.replace tbl template (job_name :: extensions))
            job_extends)
        job_extends_map ;
      tbl
    in
    let rec build_tree adjacency_tbl (path : string list) job :
        template_tree * int =
      if List.mem job path then
        error
          "A cycle exists in template extensions: %s -> %s"
          job
          (String.concat " -> " path) ;
      let path = job :: path in
      (* Job must be define in [adjacency_tbl] *)
      match Hashtbl.find adjacency_tbl job with
      | [] -> (Tree.Leaf job, 1)
      | extenders ->
          let sub_trees, children_total =
            List.fold_left
              (fun (sub_trees, children_total) job ->
                let sub_tree, children_no = build_tree adjacency_tbl path job in
                (sub_tree :: sub_trees, children_total + children_no))
              ([], 0)
              extenders
          in
          ( Tree.Node ((job, children_total), List.rev sub_trees),
            1 + children_total )
    in
    let job_is_hidden job_name =
      String.length job_name > 1 && job_name.[0] = '.'
    in
    let pp_node fmt job_name =
      Format.fprintf
        fmt
        "%s"
        (if job_is_hidden job_name then Color.(apply bold job_name)
        else job_name)
    in
    let pp_leaf fmt (job_name, _child_no) =
      Format.fprintf fmt "%a" pp_node job_name
    in
    let adjacency_tbl = if reverse then job_extends_map else job_extendee_map in
    try
      let roots =
        match roots with
        | [] ->
            (* If no roots have been specified on the command-line,
               print the tree of all jobs that have children, and all
               hidden jobs *)
            List.filter
              (fun job_name ->
                job_is_hidden job_name
                ||
                match Hashtbl.find_opt adjacency_tbl job_name with
                | Some (_ :: _) -> true
                | _ -> false)
              (Hashtbl.to_seq_keys adjacency_tbl |> List.of_seq)
        | roots ->
            List.iter
              (fun root ->
                match Hashtbl.find_opt job_extendee_map root with
                | Some _ -> ()
                | None -> error "There is no job nor template named '%s'." root)
              roots ;
            roots
      in
      let prune_trees (trees : template_tree list) : template_tree list =
        let visited : (string, unit) Hashtbl.t = Hashtbl.create 5 in
        let name : template_tree -> string = function
          | Tree.Leaf job_name | Tree.Node ((job_name, _), _) -> job_name
        in
        let rec aux tree =
          match tree with
          | Tree.Leaf job_name -> Some (Tree.Leaf job_name)
          | Tree.Node ((job_name, children_no), children) ->
              (* Nodes have already been visited are pruned -- their children are not visible. *)
              if Hashtbl.mem visited job_name then
                Some (Tree.Leaf (job_name ^ " (...)"))
              else
                let children_pruned = List.filter_map aux children in
                Hashtbl.replace visited job_name () ;
                Some (Tree.Node ((job_name, children_no), children_pruned))
        in
        List.filter_map
          (fun tree ->
            (* Omit roots that have already been visited *)
            if Hashtbl.mem visited (name tree) then None else aux tree)
          trees
      in
      (* The roots are sorted by weight such that the largest trees are printed first. *)
      let trees =
        roots
        |> List.map (build_tree adjacency_tbl [])
        |> List.sort (fun (_, c) (_, c') -> Int.compare c' c)
        |> List.map fst
      in
      let trees = if no_prune then trees else prune_trees trees in
      List.iter
        (fun tree -> Format.printf "%a" (Tree.pp_fancy pp_node pp_leaf) tree)
        trees ;
      ()
    with Failure s -> error "%s" s
end
