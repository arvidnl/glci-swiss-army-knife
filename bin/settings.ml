(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Stuff

type t = {
  gitlab_token : string option;
  gitlab_project : string option;
  environments : Config_environment.map;
  scalar_style : Yaml.scalar_style;
}

let default =
  {
    gitlab_token = None;
    gitlab_project = None;
    environments = Hashtbl.create 5;
    scalar_style = `Plain;
  }

let failwith fmt = Format.kasprintf failwith fmt

let rec find_file dir =
  let candidate = Fpath.(dir / ".gci.json") in
  if Sys.file_exists @@ Fpath.to_string candidate then Some candidate
  else if not @@ Fpath.is_root dir then find_file @@ Fpath.parent dir
  else None

let of_file file : t =
  let file_path = Fpath.to_string file in
  let json_settings =
    if Sys.file_exists file_path then
      try Yojson.Basic.from_file file_path
      with Yojson.Json_error error_message ->
        failwith
          "JSON parsing error:@.@[<h2>%s@]@.@.when reading configuration file \
           '%s'"
          error_message
          file_path
    else failwith "Specified configuration file '%s' does not exist" file_path
  in
  let with_field_opt settings field f =
    match JSON.get_field_opt json_settings field with
    | Some v -> f v
    | None -> settings
  in
  let settings = default in
  let settings =
    with_field_opt settings "gl-access-token" @@ function
    | `String s -> {settings with gitlab_token = Some s}
    | _ -> failwith "Expected 'gl-access-token' to be a string in %s" file_path
  in
  let settings =
    with_field_opt settings "gl-project" @@ function
    | `String s -> {settings with gitlab_project = Some s}
    | _ -> failwith "Expected 'gl-project' to be a string in %s" file_path
  in
  let settings =
    with_field_opt settings "scalar-style" @@ function
    | `String s -> (
        match s |> Yaml_scalar_style.of_string_opt with
        | Some scalar_style -> {settings with scalar_style}
        | None ->
            failwith
              "Expected 'scalar-style' to be one of %s"
              (String.concat ", " @@ List.map fst Yaml_scalar_style.all))
    | _ -> failwith "Expected 'scalar-style' to be a string in %s" file_path
  in
  let settings =
    with_field_opt settings "environments" @@ fun environments ->
    try
      let environments =
        Config_environment.environments_of_settings_json_exn environments
      in
      {settings with environments}
    with Failure s ->
      failwith "Unexpected format of settings.environments: %s" s
  in
  settings

let merge_environment (settings : t) : t =
  let of_env settings env_variable f =
    match Sys.getenv_opt env_variable with Some v -> f v | None -> settings
  in
  let settings =
    of_env settings "GCI_GL_ACCESS_TOKEN" @@ fun s ->
    {settings with gitlab_token = Some s}
  in
  let settings =
    of_env settings "GCI_GL_PROJECT" @@ fun s ->
    {settings with gitlab_project = Some s}
  in
  let settings =
    of_env settings "GCI_SCALAR_STYLE" @@ fun s ->
    match s |> Yaml_scalar_style.of_string_opt with
    | Some scalar_style -> {settings with scalar_style}
    | None ->
        failwith
          "Expected 'GCI_SCALAR_STYLE' to be one of %s"
          (String.concat ", " @@ List.map fst Yaml_scalar_style.all)
  in
  let settings =
    of_env settings "GCI_ENVIRONMENTS" @@ fun json_str ->
    let environments =
      try Yojson.Basic.from_string json_str
      with Yojson.Json_error error_message ->
        failwith
          "JSON parsing error:@.@[<h2>%s@]@.@.when reading environment \
           variable GCI_ENVIRONMENTS"
          error_message
    in
    try
      let environments =
        Config_environment.environments_of_settings_json_exn environments
      in
      {settings with environments}
    with Failure s -> failwith "Unexpected format of GCI_ENVIRONMENTS: %s" s
  in
  settings

let find path_opt : t =
  let path_opt : Fpath.t option =
    match path_opt with
    | Some f -> Some f
    | None -> (
        match Sys.getenv_opt "GCI_SETTINGS" with
        | Some f -> Result.to_option (Fpath.of_string f)
        | None -> (
            match find_file @@ Fpath.v (Sys.getcwd ()) with
            | Some f -> Some f
            | None -> None))
  in
  let settings =
    match path_opt with
    | None ->
        Log.debug "No settings file." ;
        default
    | Some f ->
        Log.debug "Loading settings from '%a'" Fpath.pp f ;
        of_file f
  in
  merge_environment settings
