open Stuff

let post_json ?(token : string option) ~(project : string) ~(endpoint : string)
    ~body () =
  let open Lwt.Syntax in
  let endpoint =
    Uri.(
      of_string @@ "https://gitlab.com/api/v4/projects/" ^ pct_encode project
      ^ "/" ^ endpoint)
  in
  let body = Cohttp_lwt.Body.of_string @@ Yojson.Basic.to_string body in
  let headers =
    Cohttp.Header.(add (init ()) "Content-Type" "application/json")
  in
  let headers =
    Option.fold
      ~none:headers
      ~some:(Cohttp.Header.add headers "PRIVATE-TOKEN")
      token
  in
  let* response, body_response =
    Cohttp_lwt_unix.Client.post ~headers ~body endpoint
  in
  let* body_response_string = Cohttp_lwt.Body.to_string body_response in
  Lwt.return (response, Yojson.Basic.from_string body_response_string)

type linting_response = {
  valid : bool;
  merged_yaml : string option;
  errors : string list;
  warnings : string list;
}

let linting_response_of_json json : linting_response option =
  let open Option_util in
  let open Option_util.Syntax in
  let open JSON in
  let* valid =
    get_field_opt json "valid" >>= Yojson.Basic.Util.to_bool_option
  in
  let merged_yaml =
    get_field_opt json "merged_yaml" >>= Yojson.Basic.Util.to_string_option
  in
  let of_string_list (json : Yojson.Basic.t) : string list option =
    let* ls = of_list_opt json in
    ls |> List.map Yojson.Basic.Util.to_string_option |> concat
  in
  let* errors = get_field_opt json "errors" >>= of_string_list in
  let* warnings = get_field_opt json "warnings" >>= of_string_list in
  return {valid; merged_yaml; errors; warnings}

type error =
  | Unauthorized
  | Other of (string * Yojson.Basic.t)
  | Unrecognized_json of Yojson.Basic.t
  | Unknown of Yojson.Basic.t

let lint ?token ~project ~content () =
  let open Lwt.Syntax in
  let body = `Assoc [("content", `String content)] in
  let* response, json =
    post_json ?token ~project ~endpoint:"ci/lint" ~body ()
  in
  if response.status |> Cohttp.Code.code_of_status |> Cohttp.Code.is_success
  then
    Lwt.return
    @@ Option.to_result
         ~none:(Unrecognized_json json)
         (linting_response_of_json json)
  else
    let message_opt =
      Option.bind (JSON.get_field_opt json "message") JSON.of_string_opt
    in
    let error =
      match message_opt with
      | Some "401 Unauthorized" -> Unauthorized
      | Some message -> Other (message, json)
      | None -> Unknown json
    in
    Lwt.return @@ Error error
