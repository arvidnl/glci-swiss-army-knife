# GitLab CI Inspector


GitLab CI Inspector, or `gci` for short, is a tool for
working with [GitLab CI configuration
files](https://docs.gitlab.com/ee/ci/yaml/) , e.g. `.gitlab-ci.yml`.
It can be used to lint CI configurations (using GitLab's api), to
merge multi-file CI configurations into single YAML files, to debug
template extension and comparing configurations (and much more!).

## Installation

```shell
opam install .
```

## Usage through Docker

The GitLab CI Inspector can be used through Docker. Images are
published to `registry.gitlab.com/nomadic-labs/gitlab-ci-inspector` on
each merge to master. To make it easier to use, I suggest the
following alias;


```shell
alias gci='docker run --pull always --tty --volume $(pwd):$(pwd) --workdir $(pwd) registry.gitlab.com/nomadic-labs/gitlab-ci-inspector '
```

Now the inspector can be invoked through `gci` as in the examples
below.

Note that due to the nature of the volume mapping set up by the alias,
the GitLab CI Inspector will only have access to files in the current
directory and sub-directories. Hence, the following command

```shell
gci --ci-file ../somewhere-else/.gitlab-ci.yml
```

will not work as intended.

Additionally, a Tezos-specific image is also pushed to
`registry.gitlab.com/nomadic-labs/gitlab-ci-inspector/tezos`.  This
image preloads the Tezos-specific [settings file](#settings-file) from
`example_settings/environments/gci-tezos.json`, which defines a set of
environments appropriate for working with the
[tezos/tezos](https://gitlab.com/tezos/tezos/) GitLab CI
configuration. You can set up an alias for this image like this:

```shell
alias gci-tezos='docker run --pull always --tty --volume $(pwd):$(pwd) --workdir $(pwd) registry.gitlab.com/nomadic-labs/gitlab-ci-inspector/tezos'
```

## Usage through source installation

If you're compiling the sources locally, then you can invoke the tool
through [dune](https://dune.build/):

``` shell
dune exec ./bin/main.exe
```

For ease of use, I suggest adding the following function to your shell:

``` shell
gci() {
    GCI_PATH=~/dev/nomadic-labs/gitlab-ci-inspector
    ( cd $GCI_PATH
      eval $(opam env)
      dune build ./bin/main.exe
      cd - ) >&2
    $GCI_PATH/_build/default/bin/main.exe $@
}
```

after which the inspector can be invoked through `gci` (used below).

## General usage

However you're running the GitLab CI Inspector, it is invoked as:

```shell
gci --ci-file .gitlab-ci.yml <ACTION>
```

The argument `--ci-file` denotes the entrypoint of the GitLab CI to
act upon.  It defaults to `.gitlab-ci.yml` so can be omitted to act on
the GitLab CI configuration in the working directory. It will be
omitted below.

To see the full list of actions and options, invoke:

```shell
gci man
```

or to learn more about a specific command, run

```shell
gci man merge
```

Below, some notable commands are documented in more detail.

### Merging

The main functionality is merging the CI files. This will inline all
inclusions (from the `include:` section) and output the resulting
file. Optionally, you can also inline template expansion (from
`extends:` keywords) using the `--template-extension` flag.

```shell
gci merge
```

or

```shell
gci merge --template-extension expand
```

The `merge` command, and other commands, handle conditional
includes. For more information about this functionality, see the
section [Conditional jobs and
includes](#conditional-jobs-and-includes) below.

### Linting

You can also lint a CI configuration using GitLab's Linting API.

This *requires* passing a [GitLab personal access
token](https://gitlab.com/-/profile/personal_access_tokens) with the
scope `api` through the global option `--gl-access-token <YOUR TOKEN
glpat-...>`, or by creating a settings file (see section [Settings](#settings) below).
It also requires specifying a GitLab project through the
global option `--gl-project`, or in the settings file:

```shell
gci --gl-access-token <YOUR_TOKEN glpat-...> --gl-project NAMESPACE/PROJECT lint
```

As for the merge command, `lint` supports conditional template
inclusion. See section [Conditional jobs and
includes](#conditional-jobs-and-includes) below for more information.

If you are interested in the merged YAML as reported back by GitLab's Linting API, pass the `--include-merged-yaml` to this command:

```shell
gci lint --include-merged-yaml
```

### Focus

If you want to generate or inspect the CI file for only one job, you can use the `focus` action:
```shell
gci focus <JOB_NAME>
```

For instance, given the configuration

```yaml
.foo1:
  bar1: baz1

.foo2:
  bar2: baz2

.foo3:
  bar1: baz2

main:
  extends:
    - .foo1
    - .foo2
    - .foo3
```

```shell
$ gci focus main
main:
  bar2: baz2
  bar1: baz2
```

The default template expansion strategy here inlines all extensions,
as the output will not include any templates on which the job
depends. This can be overridden using the `--template-extension` flag.

By default, the output will include non-job keys of the
configuration. That is, sections like `default`, `workflow`, etc. They
can be omitted by passing `--no-preamble`.

### Debugging template extensions

Job definitions using multi-level template expansion can be a bit
tricky to debug. The GitLab CI Inspector comes with a template extensions
debug option (invoked by passing `--template-extensions
debug-expansion` to the actions `focus` or `merge`) that will add
dummy keys to the expanded job definitions indicating the origin of a
jobs values.

Consider the following output obtained by invoking
`gci focus release-static-arm64-binaries --template-extensions debug-expansion`:

```yaml
release-static-arm64-binaries:
  __gci_extends_begin_.release_static_binaries_template_: begin
  ___gci_extends_begin_.rules_template__release_tag_: begin
  rules:
  - if: $CI_COMMIT_TAG =~ /\A\d+\.\d+\.\d+\z/ && $CI_PROJECT_NAMESPACE == "tezos"
    when: on_success
  - when: never
  ___gci_extends___end_.rules_template__release_tag_: end
  image: registry.gitlab.com/gitlab-org/release-cli
  stage: publish_release
  script:
  - apk --no-cache --virtual add bash jq curl
  - scripts/release/upload-static-binaries-to-package-registry.sh "$ARCH_PREFIX"
  __gci_extends___end_.release_static_binaries_template_: end
  __gci_job_definition: release-static-arm64-binaries
  variables:
    PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/tezos/${CI_COMMIT_TAG}
    ARCH_PREFIX: arm64-
  dependencies:
  - build_release:static-arm64-linux-binaries
```

The dummy keys are prefixed `__gci_` (the number of initial
underscores indicate template extension depth). The above result is
read thus: `release-static-arm64-binaries` has extended the
`.release_static_binaries_template` template. The result of this
template is between the dummy keys:

 - `__gci_extends_begin_.release_static_binaries_template_: begin`
 - `__gci_extends___end_.release_static_binaries_template_: end`

This template in turn extends the `rules_template__release_tag`, from
which the rules section is obtained. Then follows other values from
`.release_static_binaries_template`: `image`, `stage`, `script`. If
some key is defined by this template but overriden by a subsequent
template then it will *not* be present here, but in the expansion of
that subsequent template.

Finally, the `__gci_job_definition` dummy key indicates the start of
values that are from the job `release-static-arm64-binaries` itself.

Note that this output is *not* valid a GitLab CI configuration.

### Conditional jobs and includes

GitLab CI Inspector can be used to debug rules for jobs and inclusions.
It contains an parser and evaluator for `rules:` stanzas for both jobs
and local includes.

This allows the tool to /simulate/ pipelines. The tool can simulate
pipelines in that it does not actually execute them, it only evaluates
jobs' `rules` to and outputs the jobs that will run (and
`when`). Similarly, it can evaluates any `rules` clauses when
specified for `include:` and include or excludes the files
accordingly. It also evaluates the `workflow:` section to decide
whether a pipeline should be created at all.

The evaluation of rules is based on /environments/.

An environment is an environment name, a mapping from variables names
(including GitLab CI predefined variables such as `CI_COMMIT_BRANCH`
and user-defined variables) and a set of changed files.

Consider the environment `master` from the `example_settings/environments/gci-tezos.json` file:
```javascript
{
       "environments": {
           "master": {
               "CI_PIPELINE_SOURCE": "push",
               "CI_COMMIT_BRANCH": "master",
               "CI_COMMIT_REF_NAME": "master",
               "CI_PROJECT_NAMESPACE": "tezos",
               "__changes": ["foo.md"]
           }, // ...
       }
}
```

Its intent is to simulate a `push` pipeline on the `master` branch of
the [tezos/tezos project]((https://gitlab.com/tezos/tezos/)), that
modified the file `foo.md`, as denoted by the special key
`__changes`.  The set of changed files is used to evaluate
[`rules:changes`
keywords](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges) for jobs
and `include:`s.

Environments are configured in the [Settings](#settings).
Configured environments can be inspected with the `show
environment <ENVIRONMENT>` or `show environments` command. Both
commands take a `--format json` flag that allows you to output the
loaded environments in the JSON format of the settings file.

#### Simulating pipeline execution

Simulating a pipeline in a given environment is done like this:
```shell
gci simulate pipeline in <environment>
```

It will output the set of jobs included, their stage, their `when:`
and the variables set by the rule including the job, in a tree format.

Here is an excerpt of the output from the `master` environment from
`example_settings/environments/gci-tezos.json` in the GitLab CI
configuration of `tezos/tezos` at the time of writing:

```
$ gci simulate pipeline in master
[CI Pipeline]
├──[trigger]
│  └── [always] trigger
├──[sanity]
├──[build]
│  ├── [on_success] build:static-arm64-linux-binaries {"CI_DOCKER_HUB": "true"}
│  ├── [on_success] build:static-x86_64-linux-binaries
│  ├── [manual] build_arm64
...
```

This indicates that the `trigger` job of the `trigger` stage will run
`always`. In the stage `build`, the job
`build:static-arm64-linux-binaries` will run `on_success` with the
variables settings `{"CI_DOCKER_HUB": "true"}`, etc.

#### Simulating a single job

You can obtain more information about the inclusion of individual jobs
in a given environment with the command `simulate job <JOBNAME> in
<ENVIRONMENT>`.

Here, building on the example from the previous section, we see that
the job `build_arm64` must be manually triggered in the `master`
environment by the second rule of it's extended `rules:` keyword.

```
$ gci simulate job 'build_arm64' in master
The job 'build_arm64' would be executed 'manual' due to job rule:
 - 2:{when: manual, allow_failure: true}.
```

Hint: we can use the `focus` command to see all rules in the extended
clause:

```
$ gci focus 'build_arm64' --no-preamble | yq -y .build_arm64.rules
- if: $CI_PIPELINE_SOURCE == "schedule" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"
  when: always
- if: $CI_MERGE_REQUEST_LABELS =~ /(?:^|[,])ci--arm64(?:$|[,])/
  when: on_success
- when: manual
  allow_failure: true
```

The [yq](https://github.com/mikefarah/yq) tool is used by this example to
extract the relevant clause.

#### Simulating change-conditional rules

GitLab CI Inspector simulates
[`rules:changes`](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges)
clauses for jobs and for
[`include:`s](https://docs.gitlab.com/ee/ci/yaml/includes.html#include-with-ruleschanges).

For instance, consider a `.gitlab-ci.yml`-file that conditionally
builds a Docker image in merge request pipelines whenever a certain
`Dockerfile` is updated:

```yaml
stages: [test]
build-docker:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - "Dockerfile"
  script:
    - docker build -f Dockerfile .
```

We can simulate the triggering of this job. The evaluation of the
`rules:changes` keyword is based on a user-specified set of changed
files in the environment, through the special key `__changes`:

```javascript
{
       "environments": {
           "change-dockerfile": {
               "CI_PIPELINE_SOURCE": "merge_request_event",
               "CI_COMMIT_BRANCH": "topic-branch",
               "__changes": ["Dockerfile"]
           },
           "dont-change-dockerfile": {
               "CI_PIPELINE_SOURCE": "merge_request_event",
               "CI_COMMIT_BRANCH": "topic-branch",
               "__changes": ["README.md"]
           },
           // ...
       }
}
```

In this example we set up two environments. First, `change-dockerfile`
where the variables are setup for a merge request pipeline on the
branch `topic-branch`, and where the special key `__changes` indicate
that the `Dockerfile` has been modified in the push. Second,
`dont-change-dockerfile` which is as the former but where `Dockerfile`
is not updated.

We can now simulate the execution the job `build-docker` in these two
environments:

```
$ gci simulate job build-docker in change-dockerfile
Job 'build-docker' is triggered 'on_success' by rule 0:{if: $CI_PIPELINE_SOURCE == "merge_request_event", changes: [Dockerfile]}
$ gci simulate job build-docker in dont-change-dockerfile
Job build-docker is not triggered by rule <none>
```

Or the full pipeline:
```
$ gci simulate pipeline in dont-change-dockerfile
[CI Pipeline]
├──[test]
$ gci simulate pipeline in change-dockerfile
[CI Pipeline]
├──[test]
│  └── [on_success] build-docker
```

### Settings

GitLab CI Inspector is configured with a `.gci.json` file. This file
is searched for, in order, in the following locations:

 - In the path specified by the `--settings` global command-line
   parameter, if set.
 - In the path specified by the `GCI_SETTINGS` environment varaible,
   if set.
 - In the current working directory, and then recursive in all parent
   folders.

The settings file should be a JSON object, and may contain any of the
following fields:

 - `gl-access-token` (string): a GitLab access token, used for the
   lint command. This can be used instead of passing the
   `--gl-access-token` flag.
 - `gl-project` (string): a GitLab project, used for the lint
   command. This can be used instead of passing the `--gl-project`
   flag.
 - `scalar-style` (string): the [YAML scalar style](http://anil-code.recoil.org/ocaml-yaml/yaml/Yaml/index.html#type-scalar_style) to use in output. Must be one of:
     - `plain`
     - `single-quoted`
     - `any`
     - `double-quoted`
     - `folded`
     - `literal`
  - `enviroments` (string * (string * string object) object): defines a set of /environments/ (see section `Conditional jobs and includes` for more info).

An example configuration can be found in `example_settings/gci-example.json`:

```json
{
    "scalar-style": "double-quoted",
    "gl-access-token": "glpat-...",
    "gl-project": "tezos/tezos",

    "environments": {
        "empty-env": {},
        "schedule": {
            "CI_PIPELINE_SOURCE": "schedule"
        },
        "merge-request": {
            "CI_PIPELINE_SOURCE": "merge_request_event"
        },
        "double-pipeline": {
            "CI_COMMIT_BRANCH": "arvid@phony-branch",
            "CI_OPEN_MERGE_REQUESTS": "1234"
        },
        "push": {
            "CI_PIPELINE_SOURCE": "push"
        },

        "master": {
            "CI_PIPELINE_SOURCE": "push",
            "CI_COMMIT_BRANCH": "master",
            "CI_COMMIT_REF_NAME": "master",
            "CI_PROJECT_NAMESPACE": "tezos"
        }
    }
}
```

It sets the `scalar-style` and a phony `gl-access-token`. It sets up a
set of environments. For instance, the environment `schedule` sets the
variable `CI_PIPELINE_SOURCE` to `"schedule"`.

Settings can also be set through environment variables, in which case
they take precedence over variables loaded from a settings file.

 - `GCI_GL_TOKEN` sets the GitLab access token
 - `GCI_GL_PROJECT` sets the GitLab project
 - `GCI_ENVIRONMENTS` sets environments
 - `GCI_SCALAR_STYLE` sets the scalar style

#### Included environments

The file `example_settings/environments/gci-tezos.json` contains an
environments relevant to the [tezos/tezos
project](https://gitlab.com/tezos/tezos/). It is selected
automatically if you use the Tezos-specific image, as explained in
[Usage through Docker](#usage-through-docker).

```
gci-tezos show environments
```

If you want to modify this settings file, you can print it in JSON
format, and modify it according to your desires:

```
$ gci-tezos show environments --format json > my-environments.json
# modify my-environments.json
$ gci --settings my-environments.json show environments
```

### Included utility scripts

To aid when refactoring, the commands above can be combined to
compare two GitLab CI configurations using a set of included scripts
in the `utils` directory.

These scripts have in common that they expect that `gci` exists in
path, or that the environment variable `GCI` is configured to point to
`gci`. If you'd like to configure gci before running the
scripts, you can update `GCI` variable like so:

```
GCI='gci --settings-file ...' utils/<UTIL>.sh
```

The scripts are included in the GitLab CI Inspector Docker image. To
use them more easily, the default entrypoint of the image is a wrapper
script that either executes one of the utilities, or the `gci`
binary. If the first argument to the entrypoint is the basename of one
of the utilities, then remaining arguments are passed to the
utility. If not, all arguments are passed to the `gci` binary.

This means that if you are using GitLab CI inspector through Docker,
and you have set up the `gci` alias as proposed in [Usage through
Docker](#usage-through-docker), then you can call the utility
`utils/diff-full-config.sh` like this:

```
gci diff-full-config
```

For more information, pass `--help` to the entrypoint.

If you want to modify how gci is invoked by the `gci_util`
alias, for instance using one of the settings file included in the
image, modify the alias as such:

```
$ alias gci='docker run -v$(pwd):$(pwd) -w $(pwd) -e GCI="gci --gl-access-token <MY_ACCESS_TOKEN>" registry.gitlab.com/nomadic-labs/gitlab-ci-inspector '
```

#### Script dependencies

These scripts depends [yq](https://github.com/mikefarah/yq),
[csvkit](https://csvkit.readthedocs.io/en/latest/) and `icdiff`
(optional) among others. See [Dockerfile] for a full list of
dependencies.

Note that `yq` should be the [go-version of Mike
Farah](https://github.com/mikefarah/yq). Avoid the snap version, as
it's inability to read root files interferes the scripts usage of
temporary files.

#### `utils/diff-full-config.sh`

```
Usage: ./utils/diff-full-config.sh <ci-file-before> <ci-file-after> [<environment> [<strip-keys,...>]]

Compare the merged configuration in <ci-file-before> and
<ci-file-after> in <environment>. ...
```

This script can be used to compare the merged configurations of a given (or a set of) environment(s).
It can compare the configurations of two different worktrees, or two arbitrary git commits.
See `./utils/diff-full-config.sh --help` for more information.

#### `utils/diff-jobs.sh`
```
Usage: ./utils/diff-jobs.sh <ci-file> <job1> <job2>

Compares the extended source of two jobs in the same configuration.
```

#### `utils/diff-pipeline.sh`
```
Usage: ./utils/diff-pipeline.sh <dir-before> <dir-after> <environment>
Simulate a pipeline in the same environment in two configurations and diff the result.
```

#### `utils/diff-pipelines-report.sh`
```
Usage: ./utils/diff-pipelines-report.sh <dir-before> <dif-after> <environments-file>
Simulate a pipeline in all environments using two different configurations, diff the result per pipeline and write the result to a HTML file.
```

#### `utils/diff-pipelines.sh`
```
Usage: ./utils/diff-pipelines.sh <dir-before> <dif-after>

Simulate a pipelines in all environments using two different configurations and diff the result per pipeline.
```

#### `utils/lint-all-environments.sh`
```
Usage: ./utils/lint-all-environments.sh <dir>

Lints the GitLab CI configuration in <dir> with all available environments.
```

#### `utils/simulate-all-environments.sh`
```
Usage: ./utils/simulate-all-environments.sh [<ci_conf_dir>]

Simulates a pipeline in all defined environments. <ci_conf_dir> defaults to '.'
```

#### `utils/field-per-job.sh`

```
Usage: $0 FIELD [COMPARE_WITH]? [-- GCI_OPTIONS]

Prints the value of the FIELD for each job in the .gitlab-ci.yml
configuration in the current directory. If COMPARE_WITH is set and points
to another directory containing an .gitlab-ci.yml configuration, then the
value of FIELD in the two configurations are compared.
```

#### `utils/merge-all-envs.sh`

```
Usage: ./utils/merge-all-envs.sh

Merge all defined pipeline configuration to file .gitlab-ci.ENVIRONMENT.yml
```

#### `utils/run-pipeline.sh`

```
Usage: ./utils/run-pipeline.sh [--gl-access-token PRIVATE_TOKEN] [--gl-project PROJECT_ID] \
                               [--branch BRANCH] [--env ENV] [VAR1_NAME=VAR1_VALUE ...]

Creates a GitLab pipeline on a given branch, project, environment and variables.
```

See `./utils/run-pipeline.sh --help` for more information.

#### `utils/log-full-config.sh`

Allows to inspect the history of the merged configuration across a git
revision range, optionally in one or multiple environments.

```
Usage: ./utils/log-full-config.sh <revision-range> [<diff-full-config-params>]

Show the git commit logs of <revision-range>.

For each commit, also show the output of 'diff-full-config' for the
given commit. This diff can be parameterized by setting
<diff-full-config-params>. For more information, see 'diff-full-config
--help'.
```

### Tips and tricks

#### Comparing CI configurations

GitLab CI Inspector can help when refactoring
CI configurations. Expanding and diffing a CI configuration before and after
refactoring helps detect unwanted changes:

``` shell
gci --sort-keys --ci-file ".gitlab-ci--before.yml" merge > before.yml

gci --sort-keys --ci-file ".gitlab-ci--after.yml" merge > after.yml

diff before.yml after.yml
```

In this example, we merge two CI configurations `.gitlab-ci--before.yml`
and `.gitlab-ci--after.yml` and diff the results. We pass the
`--sort-keys` flag, which sorts the keys of YAML objects in the
output, making the diff easier to read.

#### Querying CI configurations using `jq`

The structured nature of GitLab CI configurations can be exploited to
overview large configurations. For instance, one might be interested
in reading the value of a certain key for each job in a configuration.

For example, here we extract the value of the
[interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible)
keyword for each job in a configuration:

```shell
$ gci -s plain merge -t expand \
    | yaml2json
    | jq -cr '([["Stage", "Job name", "Interruptible"]] + (to_entries | map(select( .value | type == "object")) | map(select(.value | has("stage") )) | map(select(.key | startswith(".") | not)) | map([.value.stage, .key, (.value.interruptible | tostring)])))[] | @csv'
    | csvlook
| Stage           | Job name                              | Interruptible |
| --------------- | ------------------------------------- | ------------- |
| packaging       | opam:create_pipeline                  |          True |
| packaging       | opam:trigger                          |          True |
| sanity          | sanity_ci                             |          True |
| sanity          | docker:hadolint                       |          True |
...
```

Here, we combine the inspector's `merge` command, using
template expansion, together with `yaml2json` (which is simply an alias
invoking [yq](https://github.com/mikefarah/yq) as such: `alias yaml2json='{
t=$(mktemp); cat > $t; yq . $t; rm $t; }'`),
[jq](https://stedolan.github.io/jq/) and
[csvlook](https://csvkit.readthedocs.io/en/latest/scripts/csvlook.html).
