(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Gitlab globs.

    Gitlab regular expressions appear in e.g. [changes:] expressions
    of [rules:] clauses on jobs.


    In GitLab, the are based on Ruby's globbing through
    {{:https://ruby-doc.org/core-2.5.1/File.html#method-c-fnmatch}File.fnmatch}
    with the flags [File::FNM_PATHNAME | File::FNM_DOTMATCH |
    File::FNM_EXTGLOB].

    Here, we use {!Re.Glob} with a simple transformation to handle
    [fnmatch]'s [**/*]-style globs. *)

(** A compiled GitLab glob. *)
type glob

(** Compile a glob *)
val glob : string -> glob

(** Convert a glob to a string. *)
val show : glob -> string

(** Test whether a string matches a glob.

    Example: ["foobar.ml" =~ glob "*.ml"] *)
val ( =~ ) : string -> glob -> bool

(** Negation of [=~]. *)
val ( =~! ) : string -> glob -> bool
