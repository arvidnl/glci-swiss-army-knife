open Lang

exception Error of {typ : string; message : string; value : Yaml.value}

val workflow_rules : Yaml.value -> rule list option option

val workflow : Yaml.value -> workflow option

val env : Yaml.value -> Env.t option

val job : Yaml.value -> job option
