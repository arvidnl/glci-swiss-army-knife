(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Conflang
open Conflang.Expr

let%expect_test "test parsing" =
  let pp res =
    Format.printf
      "%a"
      (Format.pp_print_option
         ~none:(fun fmt () -> Format.fprintf fmt "None")
         Expr_AST.pp)
      res
  in
  let test_parse s = pp (parse_opt s) in
  (* Literals *)
  test_parse {|"foo"|} ;
  [%expect {| (Str "foo") |}] ;
  test_parse {|'foo'|} ;
  [%expect {| (Str "foo") |}] ;
  test_parse {|'foo "bar" foo'|} ;
  [%expect {| (Str "foo \"bar\" foo") |}] ;
  test_parse {|"foo 'bar' foo"|} ;
  [%expect {| (Str "foo 'bar' foo") |}] ;
  test_parse {|/foo/|} ;
  [%expect {| (Rex /foo/) |}] ;
  test_parse {|null|} ;
  [%expect {| Null |}] ;
  (* Escaping regexp literals *)
  test_parse {|/path\/to\/somewhere/|} ;
  [%expect {| (Rex /path\/to\/somewhere/) |}] ;
  test_parse {|/\A/|} ;
  [%expect {| (Rex /\A/) |}] ;
  test_parse {|/\A\d+\.\d+\.\d+\z/|} ;
  [%expect {| (Rex /\A\d+\.\d+\.\d+\z/) |}] ;
  test_parse {|/\/path\/variable\/value$/|} ;
  [%expect {| (Rex /\/path\/variable\/value$/) |}] ;
  (* Regexps with character classes *)
  test_parse {|/va\r.*e$/|} ;
  [%expect {| (Rex /va\r.*e$/) |}] ;
  (* TODO: unsupported: \a (bell) *)
  (* test_parse {|/^v\ar.*/|} ; *)
  (* [%expect {| (Rex /^v\ar.*/) |}] ; *)
  (* Regexps with flags *)
  test_parse {|/foobar/ismU|} ;
  [%expect {| (Rex /foobar/ismU) |}] ;
  test_parse {|/foobar/i|} ;
  [%expect {| (Rex /foobar/i) |}] ;
  test_parse {|/foobar/ii|} ;
  [%expect {| (Rex /foobar/ii) |}] ;
  test_parse {|/foobar/s|} ;
  [%expect {| (Rex /foobar/s) |}] ;
  (* Comparisons *)
  test_parse {|"foo" == "bar"|} ;
  [%expect {| (Cmp ((Str "foo"), Eq, (Str "bar"))) |}] ;
  test_parse {|"foo" != "bar"|} ;
  [%expect {| (Cmp ((Str "foo"), Neq, (Str "bar"))) |}] ;
  test_parse {|"foo" =~ /bar/|} ;
  [%expect {| (Cmp ((Str "foo"), RexMatch, (Rex /bar/))) |}] ;
  test_parse {|"foo" !~ /bar/|} ;
  [%expect {| (Cmp ((Str "foo"), NrexMatch, (Rex /bar/))) |}] ;
  (* Boolean operators *)
  test_parse {|"foo" == "bar" && "bar" == "foo"|} ;
  [%expect
    {|
    (And
       ((Cmp ((Str "foo"), Eq, (Str "bar"))),
        (Cmp ((Str "bar"), Eq, (Str "foo"))))) |}] ;
  test_parse {|"foo" == "bar" || "bar" == "foo"|} ;
  [%expect
    {|
    (Or
       ((Cmp ((Str "foo"), Eq, (Str "bar"))),
        (Cmp ((Str "bar"), Eq, (Str "foo"))))) |}] ;
  test_parse {|"foo" == "bar" && "bar" == "gaz" && "gaz" == "foo"|} ;
  [%expect
    {|
    (And
       ((And
           ((Cmp ((Str "foo"), Eq, (Str "bar"))),
            (Cmp ((Str "bar"), Eq, (Str "gaz"))))),
        (Cmp ((Str "gaz"), Eq, (Str "foo"))))) |}] ;
  test_parse {|"foo" == "bar" || "bar" == "gaz" || "gaz" == "foo"|} ;
  [%expect
    {|
    (Or
       ((Or
           ((Cmp ((Str "foo"), Eq, (Str "bar"))),
            (Cmp ((Str "bar"), Eq, (Str "gaz"))))),
        (Cmp ((Str "gaz"), Eq, (Str "foo"))))) |}] ;
  (* Boolean operator associativity *)
  test_parse {|"foo" == "bar" || "bar" == "gaz" && "gaz" == "foo"|} ;
  [%expect
    {|
    (Or
       ((Cmp ((Str "foo"), Eq, (Str "bar"))),
        (And
           ((Cmp ((Str "bar"), Eq, (Str "gaz"))),
            (Cmp ((Str "gaz"), Eq, (Str "foo"))))))) |}] ;
  test_parse {|"foo" == "bar" && "bar" == "gaz" || "gaz" == "foo"|} ;
  [%expect
    {|
    (Or
       ((And
           ((Cmp ((Str "foo"), Eq, (Str "bar"))),
            (Cmp ((Str "bar"), Eq, (Str "gaz"))))),
        (Cmp ((Str "gaz"), Eq, (Str "foo"))))) |}] ;
  (* Grouping *)
  test_parse {|"foo" == "bar" && ("bar" == "gaz" || "gaz" == "foo")|} ;
  [%expect
    {|
    (And
       ((Cmp ((Str "foo"), Eq, (Str "bar"))),
        (Or
           ((Cmp ((Str "bar"), Eq, (Str "gaz"))),
            (Cmp ((Str "gaz"), Eq, (Str "foo"))))))) |}] ;
  test_parse {|("foo" == "bar" || "bar" == "gaz") && "gaz" == "foo"|} ;
  [%expect
    {|
    (And
       ((Or
           ((Cmp ((Str "foo"), Eq, (Str "bar"))),
            (Cmp ((Str "bar"), Eq, (Str "gaz"))))),
        (Cmp ((Str "gaz"), Eq, (Str "foo"))))) |}] ;
  (* Variables *)
  test_parse {|$FOOBAR|} ;
  [%expect {| (Var "FOOBAR") |}]

(* Test parsing *)
let%expect_test "test more parsing" =
  let pp ~pp_ok res =
    let ok fmt e = Format.fprintf fmt "Ok %a" pp_ok e in
    let error fmt o =
      Format.fprintf
        fmt
        "Error %a"
        (fun fmt {input; error} ->
          Format.fprintf
            fmt
            "(input=%s, err=%s)"
            input
            (Option.value ~default:"None" error))
        o
    in
    Format.printf "%a" (Format.pp_print_result ~ok ~error) res
  in
  let test_parse s = pp ~pp_ok:Expr_AST.pp (parse_res s) in

  (* Tests atom *)
  (* test_atom "1234" ;
   * [%expect {| Ok (Int 1234) |}] ; *)
  test_parse "$FOO_BAR" ;
  [%expect {| Ok (Var "FOO_BAR") |}] ;

  test_parse "\"a string\"" ;
  [%expect {| Ok (Str "a string") |}] ;

  test_parse "/a regexp/" ;
  [%expect {| Ok (Rex /a regexp/) |}] ;

  (* Tests expr *)
  test_parse "foobar" ;
  [%expect {| Error (input=foobar, err=parse error near: 'f') |}] ;

  test_parse "/test/" ;
  [%expect {| Ok (Rex /test/) |}] ;

  test_parse "$V1 == $V2" ;
  [%expect {|
              Ok (Cmp ((Var "V1"), Eq, (Var "V2"))) |}] ;

  test_parse "$V1 == $V2 && $V3 == $V4" ;
  [%expect
    {|
         Ok (And
               ((Cmp ((Var "V1"), Eq, (Var "V2"))), (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;

  test_parse "$V1 =~ /arm64/" ;
  [%expect {| Ok (Cmp ((Var "V1"), RexMatch, (Rex /arm64/))) |}] ;

  test_parse "$V1 =~ /arm64/ || $V6 =~ /arm64/" ;
  [%expect
    {|
         Ok (Or
               ((Cmp ((Var "V1"), RexMatch, (Rex /arm64/))),
                (Cmp ((Var "V6"), RexMatch, (Rex /arm64/))))) |}] ;

  test_parse "$V1 =~ /docker/ || $V6 =~ /docker/" ;
  [%expect
    {|
         Ok (Or
               ((Cmp ((Var "V1"), RexMatch, (Rex /docker/))),
                (Cmp ((Var "V6"), RexMatch, (Rex /docker/))))) |}] ;

  test_parse "$V1 =~ /opam/ || $V6 =~ /opam/" ;
  [%expect
    {|
         Ok (Or
               ((Cmp ((Var "V1"), RexMatch, (Rex /opam/))),
                (Cmp ((Var "V6"), RexMatch, (Rex /opam/))))) |}] ;

  test_parse "$V1 || $V6" ;
  [%expect {| Ok (Or ((Var "V1"), (Var "V6"))) |}] ;

  test_parse "$V7 != null && $V3 == $V4" ;
  [%expect
    {|
         Ok (And ((Cmp ((Var "V7"), Neq, Null)), (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;

  test_parse "$V7 && $V3 == $V4" ;
  [%expect
    {|
         Ok (And ((Var "V7"), (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;

  test_parse {|$V7 =~ /\A\d+\.\d+\.\d+\z/ && $V3 == $V4|} ;
  [%expect
    {|
         Ok (And
               ((Cmp ((Var "V7"), RexMatch, (Rex /\A\d+\.\d+\.\d+\z/))),
                (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;
  test_parse "$V8" ;
  [%expect {| Ok (Var "V8") |}] ;

  test_parse "$V9 =~ /(?:^|[,])ci--arm64(?:$|[,])/" ;
  [%expect
    {| Ok (Cmp ((Var "V9"), RexMatch, (Rex /(?:^|[,])ci--arm64(?:$|[,])/))) |}] ;

  test_parse "$V9 =~ /(?:^|[,])ci--docker(?:$|[,])/" ;
  [%expect
    {| Ok (Cmp ((Var "V9"), RexMatch, (Rex /(?:^|[,])ci--docker(?:$|[,])/))) |}] ;

  test_parse "$V9 =~ /(?:^|[,])ci--docs(?:$|[,])/" ;
  [%expect
    {| Ok (Cmp ((Var "V9"), RexMatch, (Rex /(?:^|[,])ci--docs(?:$|[,])/))) |}] ;

  test_parse "$V9 =~ /(?:^|[,])ci--no-coverage(?:$|[,])/" ;
  [%expect
    {| Ok (Cmp ((Var "V9"), RexMatch, (Rex /(?:^|[,])ci--no-coverage(?:$|[,])/))) |}] ;

  test_parse "$V9 =~ /(?:^|[,])ci--opam(?:$|[,])/" ;
  [%expect
    {| Ok (Cmp ((Var "V9"), RexMatch, (Rex /(?:^|[,])ci--opam(?:$|[,])/))) |}] ;

  test_parse "$V6 == $V2 && $V3 == $V4" ;
  [%expect
    {|
         Ok (And
               ((Cmp ((Var "V6"), Eq, (Var "V2"))), (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;
  test_parse "$V10 == \"parent_pipeline\"" ;
  [%expect {| Ok (Cmp ((Var "V10"), Eq, (Str "parent_pipeline"))) |}] ;
  test_parse "$V3 == \"nomadic-labs\"" ;
  [%expect {| Ok (Cmp ((Var "V3"), Eq, (Str "nomadic-labs"))) |}] ;

  test_parse "$VAR_A == \"SCHEDULE\" && $VAR_B == \"EXTENDED_TESTS\"" ;
  [%expect
    {|
         Ok (And
               ((Cmp ((Var "VAR_A"), Eq, (Str "SCHEDULE"))),
                (Cmp ((Var "VAR_B"), Eq, (Str "EXTENDED_TESTS"))))) |}] ;
  test_parse "$VAR_A == \"SCHEDULE\"" ;
  [%expect {| Ok (Cmp ((Var "VAR_A"), Eq, (Str "SCHEDULE"))) |}] ;

  test_parse "($V1 =~ /-release$/ || $V6 =~ /-release$/) && $V3 == $V4" ;
  [%expect
    {|
         Ok (And
               ((Or
                   ((Cmp ((Var "V1"), RexMatch, (Rex /-release$/))),
                    (Cmp ((Var "V6"), RexMatch, (Rex /-release$/))))),
                (Cmp ((Var "V3"), Eq, (Var "V4"))))) |}] ;
  test_parse "$V8" ;
  [%expect {| Ok (Var "V8") |}] ;

  test_parse {|"test" == ("foo" == "foo")|} ;
  [%expect
    {| Ok (Cmp ((Str "test"), Eq, (Cmp ((Str "foo"), Eq, (Str "foo"))))) |}] ;
  test_parse {|("foo" == "foo") == "test"|} ;
  [%expect
    {| Ok (Cmp ((Cmp ((Str "foo"), Eq, (Str "foo"))), Eq, (Str "test"))) |}] ;

  test_parse
    {|$CI_PIPELINE_SOURCE == "schedule" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"|} ;
  [%expect
    {|
      Ok (And
            ((Cmp ((Var "CI_PIPELINE_SOURCE"), Eq, (Str "schedule"))),
             (Cmp ((Var "TZ_SCHEDULE_KIND"), Eq, (Str "EXTENDED_TESTS"))))) |}] ;

  test_parse
    {|$CI_PIPELINE_SOURCE == "schedule" && ($TZ_SCHEDULE_KIND == "EXTENDED_TESTS")|} ;
  [%expect
    {|
      Ok (And
            ((Cmp ((Var "CI_PIPELINE_SOURCE"), Eq, (Str "schedule"))),
             (Cmp ((Var "TZ_SCHEDULE_KIND"), Eq, (Str "EXTENDED_TESTS"))))) |}] ;
  test_parse
    {|($CI_PIPELINE_SOURCE == "schedule") && ($TZ_SCHEDULE_KIND == "EXTENDED_TESTS")|} ;
  [%expect
    {|
      Ok (And
            ((Cmp ((Var "CI_PIPELINE_SOURCE"), Eq, (Str "schedule"))),
             (Cmp ((Var "TZ_SCHEDULE_KIND"), Eq, (Str "EXTENDED_TESTS"))))) |}] ;
  test_parse
    {|$CI_PROJECT_NAMESPACE == "tezos" && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^v\d+\.\d+(?:\-rc\d+)?$/|} ;
  [%expect
    "\n\
    \    Ok (And\n\
    \          ((And\n\
    \              ((Cmp ((Var \"CI_PROJECT_NAMESPACE\"), Eq, (Str \"tezos\"))),\n\
    \               (Cmp ((Var \"CI_PIPELINE_SOURCE\"), Eq, (Str \"push\"))))),\n\
    \           (Cmp\n\
    \              ((Var \"CI_COMMIT_TAG\"), RexMatch, (Rex \
     /^v\\d+\\.\\d+(?:\\-rc\\d+)?$/)))))"]

let%expect_test "test parse regexps and evaluate" =
  let test_match re_s str =
    match parse re_s with
    | Rex re -> Format.printf "%b" Re2.(str =~ re)
    | e -> Format.printf "Error, expected regexp, got: %a" Expr_AST.pp e
  in
  test_match "/foobar/" "foobar" ;
  [%expect {| true |}] ;
  test_match "/foobar/" "barfoo" ;
  [%expect {| false |}] ;
  test_match {|/path\/to\/somewhere/|} "path/to/somewhere" ;
  [%expect {| true |}] ;
  test_match {|/path\/to\/somewhere/|} "path.to.somewhere" ;
  [%expect {| false |}] ;
  test_match {|/\d+/|} "123" ;
  [%expect {| true |}] ;
  test_match {|/\A\d+\.\d+\.\d+\z/|} "12.0.13" ;
  [%expect {| true |}] ;
  test_match {|/\A\d+\.\d+\.\d+\z/|} "12.0.13-beta" ;
  [%expect {| false |}]

let%expect_test "test quoted strings" =
  (* GitLab expressions do not seem to support quoting in strings. *)
  let pp res =
    Format.printf
      "%a"
      (Format.pp_print_option
         ~none:(fun fmt () -> Format.fprintf fmt "None")
         Expr_AST.pp)
      res
  in
  let test_parse s = pp (parse_opt s) in
  test_parse {|"This is a \"sentence\" containing a quote"|} ;
  [%expect {| None |}] ;
  test_parse {|"\d+"|} ;
  [%expect {| (Str "\\d+") |}]
