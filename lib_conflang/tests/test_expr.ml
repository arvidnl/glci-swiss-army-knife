let read_file filename =
  let lines = ref [] in
  let chan = open_in filename in
  try
    while true do
      lines := input_line chan :: !lines
    done ;
    !lines
  with End_of_file ->
    close_in chan ;
    List.rev !lines

type expected_result = Unknown | Included | Excluded | Error

type line =
  | Comment of string
  | Var of string * string
  | Rule of {name : string; rule : string; expected_result : expected_result}

let test_expressions () =
  let parse_line line =
    let open Conflang.Re2 in
    let re_comment = compile {|^#.*$|} in
    let re_var = compile {|^(\w+)=(.*)|} in
    let re_rule = compile {|(.*): '(.*)'(| # --> (true|false|error))$|} in

    if line =~ re_comment then Comment line
    else
      match line =~** re_var with
      | Some (var_name, var_value) -> Var (var_name, var_value)
      | None -> (
          match line =~^ re_rule with
          | Some groups ->
              let name = groups.(1) in
              let rule = groups.(2) in
              let expected_result = groups.(4) in
              let expected_result =
                match expected_result with
                | "true" -> Included
                | "false" -> Excluded
                | "error" -> Error
                | _ -> Unknown
              in
              Rule {name; rule; expected_result}
          | None ->
              Stdlib.failwith (Format.asprintf "Could not parse line: %s" line))
  in

  let pp_eval_res fmt res =
    Format.fprintf
      fmt
      "%a"
      (Format.pp_print_option
         ~none:(fun fmt () -> Format.fprintf fmt "None")
         Format.pp_print_bool)
      res
  in

  let env = Hashtbl.create 5 in
  let res = ref true in
  List.iteri
    (fun line_no line ->
      match parse_line line with
      | Comment _ -> ()
      | Var (x, v) -> Hashtbl.replace env x v
      | Rule {name = rule_name; rule = rule_str; expected_result} -> (
          match Conflang.Expr.parse rule_str with
          | exception Conflang.Expr.Parse_error _ -> (
              match expected_result with
              | Error -> ()
              | _ ->
                  Format.printf
                    "Line %d, could not parse rule `%s` (ex)\n"
                    line_no
                    rule_str ;
                  exit 1)
          | rule ->
              let eval_result = Conflang.Expr_eval.eval_bool env rule in
              let test_result, message =
                match (expected_result, eval_result) with
                | Unknown, _ ->
                    ( true,
                      Format.asprintf "evaluated to: %a" pp_eval_res eval_result
                    )
                | Included, Some true -> (true, "included as expected")
                | Excluded, Some false -> (true, "excluded as expected")
                | Error, None -> (true, "errored as expected")
                | Included, Some false -> (false, "was unexpectedly excluded")
                | Excluded, Some true -> (false, "was unexpectedly included")
                | (Included | Excluded), None -> (false, "unexpectedly errored")
                | Error, Some _ -> (false, "was unexpectedly did not error")
              in
              if not test_result then res := false ;
              Format.printf
                "%3d. [%s] The rule %s: %s, %s%s\n"
                (line_no + 1)
                (if test_result then "✓" else "✘")
                rule_name
                rule_str
                message
                (if Conflang.Env.is_empty env then
                 Format.asprintf " (env: %a)" Conflang.Env.pp env
                else "")))
    (read_file "test_exprs.txt") ;

  if !res then print_endline "All good!"
  else (
    print_endline "There were failures." ;
    exit 1)

let () = test_expressions ()
