(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Gitlab regular expressions.

    Gitlab regular expressions appear in e.g. [if:] expressions of
    [rules:] clauses on jobs. Under the hood, they are
    based on {{:https://github.com/google/re2/wiki/Syntax} RE2}. *)

(** A compiled Gitlab regular expression *)
type t

(** Gitlab regular expression options.

    Options correspond to Re2 flags in the following manner:

    - ['i'] -> [`Caseless]
    - ['s'] -> [`Dotall]
    - ['m'] -> [`Multiline]
    - ['U'] -> [`Ungreedy]

    They are translated to the {!Re.Perl.opt} equivalent. *)
type opt = [`Caseless | `Dotall | `Multiline | `Ungreedy]

(** Compile a regular expression using Perl syntax.

    Note: The string argument should not be enclosed with forward slashes. *)
val compile : ?opts:opt list -> string -> t

(** Compile a regular expression using Perl syntax with flags.

    Note: The string argument should not be enclosed with forward slashes.

    [flags] correspond to the suffix [is] of a pattern like
    [/foobar/is]. See {!opt} for the interpretation such flags. *)
val compile_flags : flags:string -> string -> t

(** Pretty-print a regular expression enclosed with forward slashes. *)
val pp : Format.formatter -> t -> unit

(** Test whether a string matches a regular expression.

    Example: ["number 1234 matches" =~ rex "\\d+"] *)
val ( =~ ) : string -> t -> bool

(** Negation of [=~]. *)
val ( =~! ) : string -> t -> bool

(** Match a regular expression with one capture group. *)
val ( =~* ) : string -> t -> string option

(** Match a regular expression with two capture groups. *)
val ( =~** ) : string -> t -> (string * string) option

(** Match a regular expression with three capture groups. *)
val ( =~*** ) : string -> t -> (string * string * string) option

(** Match a regular expression and return all capture groups. *)
val ( =~^ ) : string -> t -> string array option

(** The regular expression that matches itself literally. *)
val literal : string -> t
