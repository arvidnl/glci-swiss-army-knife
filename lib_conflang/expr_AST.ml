(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(* https://docs.gitlab.com/ee/ci/jobs/job_control.html#cicd-variable-expressions *)

(** Comparison operators *)
type comparison_op = Eq | Neq | RexMatch | NrexMatch
[@@deriving show {with_path = false}]

(** AST of GitLab CI expressions *)
type t =
  | And of (t * t)
  | Or of (t * t)
  | Cmp of (t * comparison_op * t)
  | Var of string
  | Str of string
  | Rex of Re2.t
  | Null
[@@deriving show {with_path = false}]
