/*****************************************************************************/
/*                                                                           */
/* SPDX-License-Identifier: MIT                                              */
/* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                */
/*                                                                           */
/*****************************************************************************/

%{
open Expr_AST

let re2_compile (pattern, flags) = Re2.compile_flags ~flags pattern

%}

%token <string> STRING
%token <string * string> RE
%token <string> VAR
%token NULL EQUAL NOT_EQUAL MATCHES NOT_MATCHES LPAR RPAR NOT OR AND EOF

%left OR
%left AND
%nonassoc EQUAL MATCHES NOT_EQUAL NOT_MATCHES

%type <Expr_AST.t> expression
%start expression
%%

expression:
| expr EOF
  { $1 }

expr:
| expr AND expr
  { And ($1, $3) }
| expr OR expr
  { Or ($1, $3) }
| NULL
  { Null }
| STRING
  { Str $1 }
| RE
  { Rex (re2_compile $1) }
| VAR
  { Var $1 }
| expr EQUAL expr
  { Cmp ($1, Eq, $3) }
| expr NOT_EQUAL expr
  { Cmp ($1, Neq, $3) }
| expr MATCHES expr
  { Cmp ($1, RexMatch, $3) }
| expr NOT_MATCHES expr
  { Cmp ($1, NrexMatch, $3) }
| LPAR expr RPAR
  { $2 }
