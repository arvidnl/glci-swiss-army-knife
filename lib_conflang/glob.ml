(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

type glob = string * Re.re

let to_gitlab_glob g =
  (* - replace '**[^/]?' with '*'
     - replace '**/' with '{**/,}' *)
  let g_len = String.length g in
  let buf = Buffer.create g_len in
  let i = ref 0 in
  let prec = ref None in
  while !i < g_len do
    match g.[!i] with
    | '*' -> (
        (* If this is the last character: add to buffer.
           If this is the second to last character, and the following is '*': add '*' to buffer. *)
        match g_len - !i with
        | 1 ->
            (* this is the last character, add '*' to buffer *)
            Buffer.add_char buf '*' ;
            i := !i + 1
        | 2 ->
            if g.[!i + 1] = '*' then (
              Buffer.add_char buf '*' ;
              i := !i + 2)
            else (
              Buffer.add_char buf '*' ;
              i := !i + 1)
        | _ -> (
            match (!prec, g.[!i + 1], g.[!i + 2]) with
            | (None | Some '/'), '*', '/' ->
                Buffer.add_string buf "{**/,}" ;
                i := !i + 3
            | _, '*', _c ->
                Buffer.add_char buf '*' ;
                i := !i + 2
            | _, _c1, _c2 ->
                Buffer.add_char buf '*' ;
                i := !i + 1))
    | c ->
        Buffer.add_char buf c ;
        i := !i + 1 ;
        prec := Some c
  done ;
  Buffer.contents buf

let glob ?anchored ?pathname ?match_backslashes ?period ?expand_braces
    ?double_asterisk glob =
  let re =
    Re.Glob.glob
      ?anchored
      ?pathname
      ?match_backslashes
      ?period
      ?expand_braces
      ?double_asterisk
      glob
  in
  (glob, Re.compile re)

let glob g =
  glob ~anchored:true ~expand_braces:true ~period:false (to_gitlab_glob g)

let show (s, _) = s

let ( =~ ) s (_, g) = Re.execp g s

let ( =~! ) s (_, g) = not (Re.execp g s)

(** {2 Tests} *)

let%expect_test "test globs rewrite" =
  let f g = Printf.printf "%s\n" (to_gitlab_glob g) in
  f "**/*.rb" ;
  [%expect {| {**/,}*.rb |}] ;
  f "**.rb" ;
  [%expect {| *.rb |}] ;
  f "foobar_**/main.rb" ;
  [%expect {| foobar_*/main.rb |}] ;
  f "foo/**/main.rb" ;
  [%expect {| foo/{**/,}main.rb |}]
