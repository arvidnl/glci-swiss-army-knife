open Lang
open Stuff

(*

 Convention:

  functions in this module should obey;

  - if the passed yaml value does not follow the format of the given parser (e.g. a missing required field): raise exception

*)

exception Error of {typ : string; message : string; value : Yaml.value}

let dbg typ ?pp f v =
  let pp =
    Option.value
      ~default:(fun fmt _ -> Format.pp_print_string fmt "Some <?>")
      pp
  in
  Log.debug
    "Yaml <%a> evaluated to %s |%a|\n"
    Yaml.pp
    v
    typ
    (Format.pp_print_option
       ~none:(fun fmt () -> Format.pp_print_string fmt "<None>")
       pp)
    (f v) ;
  f v

(* let dbg _typ ?pp f v =
 *   ignore pp ;
 *   f v *)

let env : Yaml.value -> Env.t option =
 fun v ->
  let open Option_util.Syntax in
  let* bindings = YAML.(v |> of_object_opt) in
  let value_of_scalar_opt = function
    | `String s -> Some s
    | `Float f -> Some (string_of_float f)
    | `Bool true -> Some "true"
    | `Bool false -> Some "false"
    | _ -> None
  in
  let* bindings =
    List.map
      (fun (k, v) ->
        ( k,
          match v with
          | `String _ | `Float _ | `Bool _ -> value_of_scalar_opt v
          | `O _ -> YAML.(v |-> "value" |> value_of_scalar_opt)
          | _ -> None ))
      bindings
    |> Option_util.concat_assoc
  in
  return (Env.of_list bindings)

let env = dbg "env" ~pp:Env.pp env

let ( <$> ) : (Yaml.value -> 'a option) -> 'a -> Yaml.value -> 'a option =
 fun f d v -> match v with `Null -> Some d | v -> f v

let when_ : Yaml.value -> when_val option option =
  let open Option_util.Syntax in
  fun v ->
    match YAML.(v |> of_string_opt) with
    | Some "on_success" -> return (Some On_success)
    | Some "on_failure" -> return (Some On_failure)
    | Some "always" -> return (Some Always)
    | Some "manual" -> return (Some Manual)
    | Some "delayed" -> return (Some Delayed)
    | Some "never" -> return (Some Never)
    | None -> return None
    | Some s -> fail ("unknown when value: " ^ s)

let changes : Yaml.value -> changes option =
  let open Option_util.Syntax in
  let parse_path v = Glob.glob (YAML.of_string_exn v) in
  function
  | `A paths ->
      let paths = paths |> List.map parse_path in
      return {paths; compare_to = None}
  | `O _ as changes ->
      let paths =
        YAML.(changes |-> "paths" |> of_array_exn |> List.map parse_path)
      in
      let compare_to = YAML.(changes |-> "compare_to" |> of_string_opt) in
      return {paths; compare_to}
  | v ->
      fail
        ("unexpected type of `changes` clause: " ^ YAML.tag v
       ^ ", expected string array or option.")

let rule : Yaml.value -> rule option =
 fun value ->
  let open Option_util.Syntax in
  let* when_ = YAML.(value |-> "when" |> when_) in
  let* (variables : Env.t) =
    YAML.(value |-> "variables" |> (env <$> Env.empty ()))
  in
  let if_ =
    match YAML.(value |-> "if" |> of_string_opt) with
    | None -> None
    | Some if_expression -> (
        try Some (Expr.parse if_expression)
        with Expr.Parse_error {input; error} ->
          let message =
            Format.asprintf
              "Parse error raised when parsing 'if:' of 'rule:', could not \
               parse '%s' in expression '%s'%s"
              input
              if_expression
              (match error with Some e -> ": " ^ e | None -> "")
          in
          raise (Error {typ = "rule"; message; value}))
  in
  let changes =
    if YAML.(has "changes" value) then changes YAML.(value |-> "changes")
    else None
  in
  return
    {
      if_;
      changes;
      when_;
      allow_failure = YAML.(value |-> "allow_failure" |> of_bool_opt);
      variables;
      source__ = Some value;
      index__ = None;
    }

let rule = dbg "rule" ~pp:pp_rule rule

let rules : Yaml.value -> rule list option option =
 fun v ->
  let open Option_util.Syntax in
  match YAML.(v |> of_array_opt) with
  | None -> return None
  | Some rules ->
      let+ rules =
        List.mapi
          (fun i v ->
            let+ rule = rule v in
            {rule with index__ = Some i})
          rules
        |> Option_util.concat
      in
      Some rules

let workflow_rule : Yaml.value -> rule option =
 fun value ->
  let open Option_util.Syntax in
  let* rule = rule value in
  match rule with
  | {when_ = None | Some Always | Some Never; _} -> return rule
  | _ ->
      raise
        (Error
           {
             typ = "workflow_rule";
             message =
               "when: can only be 'always' or 'never' when used with workflow.";
             value;
           })

let workflow_rule = dbg "workflow_rule" ~pp:pp_rule workflow_rule

let workflow_rules v =
  let open Option_util.Syntax in
  match YAML.(v |> of_array_opt) with
  | None -> return None
  | Some rules ->
      let+ rules = List.map workflow_rule rules |> Option_util.concat in
      Some rules

let workflow : Yaml.value -> workflow option =
 fun v ->
  let open Option_util.Syntax in
  let* rules = YAML.(v |-> "rules" |> workflow_rules) in
  match rules with
  | None | Some [] ->
      fail "a workflow, if defined, must have a non-empty set of rules"
  | Some rules -> return {rules}

let workflow = dbg "workflow" ~pp:pp_workflow workflow

let of_string_list_opt v =
  YAML.(v |> of_array |> List.map of_string_opt |> Option_util.concat)

let job_allow_failure : Yaml.value -> job_allow_failure option option =
  let open Option_util.Syntax in
  function
  | `Null -> return None
  | `O [("exit_codes", `Float code)] ->
      return (Some (Allow_on_exit_codes [int_of_float code]))
  | `O [("exit_codes", `A codes)] ->
      let+ codes =
        codes
        |> List.map (fun v -> v |> YAML.of_float_opt)
        |> Option_util.concat
      in
      let codes = List.map int_of_float codes in
      Some (Allow_on_exit_codes codes)
  | `Bool true -> return (Some Always_allow)
  | `Bool false -> return (Some Never_allow)
  | _ -> return None

let job : Yaml.value -> job option =
 fun v ->
  let open Option_util.Syntax in
  let* stage =
    (* If no stage is defined, then the default is "test" *)
    YAML.(v |-> "stage" |> (of_string_opt <$> "test"))
  in
  let* dependencies =
    match YAML.(v |-> "dependencies") with
    | `Null -> return None
    | dependencies ->
        let* dependencies = of_string_list_opt dependencies in
        return (Some dependencies)
  in
  let* extends = YAML.(v |-> "extends" |> of_string_list_opt) in
  let* after_script = YAML.(v |-> "before_script" |> of_string_list_opt) in
  let* script = YAML.(v |-> "script" |> of_string_list_opt) in
  let* before_script = YAML.(v |-> "after_script" |> of_string_list_opt) in
  let coverage = YAML.(v |-> "coverage" |> of_string_opt) in
  let* allow_failure = YAML.(v |-> "allow_failure" |> job_allow_failure) in
  let* rules = YAML.(v |-> "rules" |> rules) in
  let* variables = YAML.(v |-> "variables" |> (env <$> Env.empty ())) in
  let* when_ = YAML.(v |-> "when" |> when_) in
  return
    {
      stage;
      extends;
      dependencies;
      before_script;
      script;
      after_script;
      coverage;
      allow_failure;
      rules;
      variables;
      when_;
    }

let job = dbg "job" ~pp:Lang.pp_job job
