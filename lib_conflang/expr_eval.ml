(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Expr_AST

type value = S of string | R of Re2.t | B of bool | Null
[@@deriving show {with_path = false}]

let value_as_bool : value -> bool = function
  | S _ | R _ -> true
  | B b -> b
  | Null -> false

let value_is_truthful : value -> bool = function
  | S "" -> false
  | S _ | R _ -> true
  | B b -> b
  | Null -> false

exception Invalid_comparison

let eq v1 v2 =
  match (v1, v2) with
  (* Strings *)
  | S s1, S s2 -> s1 = s2
  | S _, R _ -> false
  | S _, B _ -> false
  | S _, Null -> false
  (* Regular expressions *)
  | R _, S _ -> raise Invalid_comparison
  | R _, B _ -> raise Invalid_comparison
  | R r1, R r2 -> r1 = r2
  | R _, Null -> false
  (* Booleans *)
  | B _, S _ -> false
  | B _, R _ -> false
  | B b1, B b2 -> b1 = b2
  | B _, Null -> false
  (* Null *)
  | Null, S _ -> false
  | Null, R _ -> false
  | Null, B _ -> false
  | Null, Null -> true

let rexmatch v1 v2 =
  (* https://docs.gitlab.com/ee/ci/jobs/job_control.html#compare-a-variable-to-a-regex-pattern *)
  let value_to_string = function
    | S s -> s
    | R _r -> failwith "The string value of regular expressions is not defined"
    | B b -> Bool.to_string b
    | Null -> ""
  in
  (* https://docs.gitlab.com/ee/ci/jobs/job_control.html#store-the-regex-pattern-in-a-variable *)
  let v2 =
    match v2 with
    | S s -> (
        match Expr.parse_opt s with
        | Some (Expr_AST.Rex re) -> R re
        | Some _ | None -> v2)
    | _ -> v2
  in
  match (v1, v2) with
  | R _, _ ->
      (* As far as I've understood, regexps do not match any regexps *)
      false
  | v1, S s2 ->
      (* checks if the left side (s1) is a substring of the right side (s2). *)
      Re2.(s2 =~ literal (value_to_string v1))
  | v1, R r -> Re2.(value_to_string v1 =~ r)
  | _, B _ -> raise Invalid_comparison
  | _, Null -> false

let rec eval (env : Env.t) : Expr_AST.t -> value = function
  | And (e1, e2) ->
      let v1 = eval env e1 in
      let v2 = eval env e2 in
      if value_as_bool v1 then v2 else v1
  | Or (e1, e2) ->
      let v1 = eval env e1 in
      let v2 = eval env e2 in
      if value_as_bool v1 then v1 else v2
  | Cmp (e1, cmp, e2) ->
      let v1 = eval env e1 in
      let v2 = eval env e2 in
      let bv =
        match cmp with
        | Eq -> eq v1 v2
        | Neq -> not (eq v1 v2)
        | RexMatch -> rexmatch v1 v2
        | NrexMatch -> not (rexmatch v1 v2)
      in
      B bv
  | Var x ->
      let none = Null in
      Option.fold ~none ~some:(fun s -> S s) (Hashtbl.find_opt env x)
  | Str s -> S s
  | Rex r -> R r
  | Null -> Null

let eval env e = try Some (eval env e) with Invalid_comparison -> None

let eval_bool env e = Option.map value_is_truthful (eval env e)
