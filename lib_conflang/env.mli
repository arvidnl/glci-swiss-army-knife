(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** An environment is a set of variables and their values. *)
type t = (string, string) Hashtbl.t

(** Create an environment from a list. *)
val of_list : (string * string) list -> t

(** Obtain the bindings of an environment as a list. *)
val to_list : t -> (string * string) list

(** [is_empty e] is [true] if [t] contains no bindings *)
val is_empty : t -> bool

(** Create an empty environment *)
val empty : unit -> t

(** Pretty print an environment *)
val pp : Format.formatter -> t -> unit

(** Merge two environments, preferring values from the second environment on conflicts. *)
val merge : t -> t -> unit
