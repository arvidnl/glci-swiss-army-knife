(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** GitLab CI expression language.


    {{:https://docs.gitlab.com/ee/ci/jobs/job_control.html#cicd-variable-expressions}The
    GitLab CI expression language} is used in [if:] expressions of
    e.g. [rules:] clauses in job definitions. This module provides a
    parser for such expressions. See {!Expr_AST} for the AST of
    expressions and its pretty printer. See {!Expr_eval} for the
    semantics. *)

type parse_error = {
  input : string;  (** Input of the parsing. *)
  error : string option;
      (** Contains an optional error message from the lexer or parser. *)
}

(* Raised by {!parse} on parse errors. *)
exception Parse_error of parse_error

(* Parse a GitLab CI expression. *)
val parse : string -> Expr_AST.t

(* Parse a GitLab CI expression as a {!result}. *)
val parse_res : string -> (Expr_AST.t, parse_error) result

(* Parse a GitLab CI expression as an {!option}. *)
val parse_opt : string -> Expr_AST.t option
