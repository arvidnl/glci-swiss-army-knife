#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

require_command jq
require_command ansi2html

if [ "${1:-}" = "--help" ] || [ -z "${1:-}" ] || [ -z "${2:-}" ] || [ -z "${3:-}" ]; then
  echo "Usage: $0 <dir-before> <dif-after> <environments-file>"

  echo "Simulate a pipeline in all environments using two different configurations, diff the result per pipeline and write the result to a HTML file."
  exit 1
fi

if ! [ -f "$3" ]; then
  echo "Environment file $3 does not exist."
  exit 1
fi

before=$1
after=$2
environments_file=$3

{
  "$SCRIPT_DIR"/diff-pipelines.sh "$before" "$after" "$environments_file"
  echo "── All environments: ───────────────────────────────────────────────────────────"
  jq .environments < "$environments_file"
} | ansi2html --white --title "gci: Comparing pipelines in $before and $after"
