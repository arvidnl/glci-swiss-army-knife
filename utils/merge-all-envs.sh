#!/bin/sh

set -eu

if [ "${TRACE:-}" = 1 ]; then set -x; fi

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

require_command jq

if [ "${1:-}" = "--help" ]; then
  echo "Usage: $0"
  echo
  echo "Merge all defined pipeline configuration to file .gitlab-ci.ENVIRONMENT.yml"
  exit 1
fi

environments_file=$(mktemp)
$GCI show environments --format json > "$environments_file"
if ! jq empty < "$environments_file"; then
  echo "An invalid environment file was generated. This might indicate that your GCI setting (currently, '$GCI') is invalid. Output: "
  cat "$environments_file"
  exit 1
fi
env_tot=$(jq -r '.environments|keys|length' < "$environments_file")
if [ "$env_tot" -eq 0 ]; then
  echo "The current settings define no environments: "
  cat "$environments_file"
  exit 1
fi

for environment in $(jq -r '.environments|keys[]' < "$environments_file"); do
  $GCI --sort-keys merge \
    -e "$environment" -t expand \
    --exclude-untriggered-jobs --exclude-templates > "$(pwd)"/.gitlab-ci."$environment".yml
  echo "Wrote $(pwd)/.gitlab-ci.$environment.yml"
done
