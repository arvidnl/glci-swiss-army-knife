#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ] || [ $# -lt 2 ]; then
  cat << EOT
Usage: $0 <ci-file-before> <ci-file-after> [<environment> [<strip-keys,...>]]

Compare the merged configuration in <ci-file-before> and
<ci-file-after> in <environment>. <ci-file-after>/<ci-file-before> can be
either:
 - The path to a '.gitlab-ci.yml'-file
 - The path to a folder that contains '.gitlab-ci.yml'-file
 - A git reference.

In the last case, the git reference is exported to a temporary folder,
from which a merged configuration is produced. This means that you can
do e.g.:

  $0 HEAD . before_merging

to compare the state of the working tree with the state of the most
recent commit. Two refs can be compared in this way:

  $0 HEAD HEAD^ before_merging

This will show the differences in the merged configuration between the
most recent commit and its predecessor.

<environment> should be one or several comma-separated environments
specified in the .gci.json settings file. If set to the special value
'all-envs' then all environments are compared. If omitted, or set to
the special value 'none', then no environment is loaded (no
'--evaluation-strategy' argument is passed to GCI).

<strip-keys> is an optional comma-separated list of job fields to
exclude in the comparison (e.g. 'rules').

The program used to compare merged configurations can be modified by
setting the environment variable DIFF.
EOT
  exit 1
fi

tmp_dirs=""
cleanup() {
  if [ -n "${tmp_dirs:-}" ]; then
    # shellcheck disable=SC2086
    rm -rf $tmp_dirs
  fi
}
trap cleanup EXIT INT

merge_environment() {
  f_ci_file=$1
  f_environment=$2
  if [ -n "$3" ]; then
    f_strip_keys="--strip-job-keys $3"
  else
    f_strip_keys=
  fi

  # shellcheck disable=SC2086
  $GCI --sort-keys --ci-file "$f_ci_file" merge \
    $f_strip_keys \
    -e "$f_environment" -t expand --exclude-templates
}

git_export_maybe() {
  f_ci_file=$1
  git_log=$(mktemp -p /tmp gci-export-git-log.XXXXXX)
  tmp_dirs="$tmp_dirs $git_log"
  if [ -d "${f_ci_file}" ] || [ -f "${f_ci_file}" ]; then
    # If file
    echo "${f_ci_file}"
  elif git cat-file -t "$f_ci_file" > "${git_log}" 2>&1; then
    # This is a commit

    # For unclear reasons, running a status avoids diff-index
    # reporting that there is a diff in .gitattributes although
    # there isn't any...
    git status > /dev/null 2>&1

    if git diff-index --quiet HEAD -- .gitattributes; then
      tmp=$(mktemp -p /tmp -d tmp.gci-export.XXXXXX)
      tmp_dirs="$tmp_dirs $tmp"

      paths=".gitlab-ci.yml"
      if [ -d ./.gitlab ]; then
        paths="$paths ./.gitlab"
      fi

      # Clear .gitattributes if present to ensure the
      # '.gitlab-ci.yml' files are not excluded form the
      # archive.
      if [ -f .gitattributes ]; then
        mv .gitattributes .gitattributes.bak
        echo "" > .gitattributes
      fi

      # shellcheck disable=SC2086
      git archive --worktree-attributes "$f_ci_file" $paths | tar x -C "${tmp}"

      # Restore '.gitattributes' if it was present.
      if [ -f .gitattributes.bak ]; then
        mv .gitattributes.bak .gitattributes
      fi

      echo "${tmp}"
    else
      echo ".gitattributes is dirty, cannot continue" >&2
      echo ""
    fi
  else
    "${f_ci_file} is neither an existing file or commit"
    if [ -n "$(cat "${git_log}")" ]; then
      echo "Git says: $(cat "${git_log}")"
    fi
    exit 1
  fi
}

ci_file_before=$1
ci_file_after=$2
environments=${3:-}
strip_keys=${4:-}

DIFF=${DIFF:-diff -r --show-c-function --color=always -u}

ci_file_before=$(git_export_maybe "${ci_file_before}")
if [ -z "$ci_file_before" ]; then exit 1; fi
ci_file_after=$(git_export_maybe "${ci_file_after}")
if [ -z "$ci_file_after" ]; then exit 1; fi

config_before=$(mktemp -p /tmp -d tmp.gci-diff-before.XXXXXX)
tmp_dirs="$tmp_dirs $config_before"
config_after=$(mktemp -p /tmp -d tmp.gci-diff-after.XXXXXX)
tmp_dirs="$tmp_dirs $config_after"

if [ "${environments}" = "all-envs" ]; then
  environments_file=$(mktemp)
  $GCI show environments --format json > "$environments_file"
  if ! jq empty < "$environments_file"; then
    echo "An invalid environment file was generated. This might indicate that your GCI setting (currently, '$GCI') is invalid. Output: "
    cat "$environments_file"
    exit 1
  fi
  env_tot=$(jq -r '.environments|keys|length' < "$environments_file")
  if [ "$env_tot" -eq 0 ]; then
    echo "The current settings define no environments: "
    cat "$environments_file"
    exit 1
  fi
  environments=$(jq -r '.environments|keys[]' < "$environments_file")
fi

if [ -z "${environments}" ] || [ "${environments}" = "none" ]; then
  $GCI --sort-keys --ci-file "$ci_file_before" merge -t expand \
    > "${config_before}/.gitlab-ci.yml"
  $GCI --sort-keys --ci-file "$ci_file_after" merge -t expand \
    > "${config_after}/.gitlab-ci.yml"
  $DIFF "${config_before}/.gitlab-ci.yml" "${config_after}/.gitlab-ci.yml"
else
  for environment in $(echo "$environments" | tr "," "\n"); do
    merge_environment "$ci_file_before" "$environment" "$strip_keys" \
      > "${config_before}/.gitlab-ci.${environment}.yml"
    merge_environment "$ci_file_after" "$environment" "$strip_keys" \
      > "${config_after}/.gitlab-ci.${environment}.yml"
  done
  $DIFF "${config_before}" "${config_after}"
fi
