#!/bin/sh

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ] || [ -z "${1:-}" ] || [ -z "${2:-}" ] || [ -z "${3:-}" ]; then
  echo "Usage: $0 <ci-file> <job1> <job2>"
  echo
  echo "Compares the extended source of two jobs in the same configuration."
  exit 1
fi

ci_file="$1"
job1="$2"
job2="$3"

tmp1=$(mktemp)
$GCI --ci-file "$ci_file" focus "$job1" --no-preamble > "$tmp1"

tmp2=$(mktemp)
$GCI --ci-file "$ci_file" focus "$job2" --no-preamble > "$tmp2"

diff --color -u "$tmp1" --label "$job1" "$tmp2" --label "$job2"
