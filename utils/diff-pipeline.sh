#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ] || [ -z "${1:-}" ] || [ -z "${2:-}" ] || [ -z "${3:-}" ]; then
  echo "Usage: $0 <dir-before> <dir-after> <environment>"

  echo "Simulate a pipeline in the same environment in two configurations and diff the result."
  exit 1
fi

before=$1
after=$2
environment=$3

before_file=$(mktemp)
after_file=$(mktemp)

$GCI --ci-file "$before/" simulate pipeline in "$environment" > "$before_file"
$GCI --ci-file "$after/" simulate pipeline in "$environment" > "$after_file"

if ! diff -q "$before_file" "$after_file" > /dev/null; then
  $ICDIFF "$before_file" --label "$before" "$after_file" --label "$after"
  exit 1
else
  echo "The two pipelines for environment '$environment' in configuration '$before' and '$after' are identical."
  exit 0
fi
