#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

usage() {
  cat << EOT
Usage: $0 [--gl-access-token GCI_GL_ACCESS_TOKEN] [--gl-project GCI_GL_PROJECT]
          [--branch BRANCH] [--env ENV] [VAR1_NAME=VAR1_VALUE ... |-]
Creates a GitLab CI pipeline on a given branch, project, environment and
variables.

Example:
  $0 --gl-access-token gl-... CLEAN_OPAM_CACHE=true CLEAN_DUNE_CACHE=true

GCI_GL_ACCESS_TOKEN, GCI_GL_PROJECT and BRANCH can also be set through the
environment variables of the same name.  A GitLab private token must
be supplied, either through --gl-access-token or GCI_GL_ACCESS_TOKEN.
By default, the BRANCH is current branch of the repo in the current
working directory, if any.  Likewise, the the script will attempt to
infer the GitLab project from the remote/pushRemote of the current
branch.

If provided with '--env', <ENV> should be an environments specified in
the .gci.json settings file. Corresponding variables (excluding the
special '__changes' variable) will be set for the created pipeline.
If both '--env ENV' and VAR_NAME=VAR_VALUE is provided, the latter
variables take precedence.

Calling this utility with no arguments will produce this help message.
If you want to create a pipeline without setting any variables or
other arguments, pass '-' instead of variables to inhibit this help
message:

  $0 -
EOT
  exit 1
}

uri_encode() {
  jq -rn --arg x "$1" '$x|@uri'
}

git_branch() {
  if [ -d .git ] || [ -f .git ]; then
    git rev-parse --abbrev-ref HEAD
  fi
}

gitlab_project_of() {
  if [ -d .git ] || [ -f .git ]; then
    br=$(git_branch)
    remote=$(git config --get branch."$br".pushRemote)
    if [ -z "$remote" ]; then
      remote=$(git config --get branch."$br".remote)
    fi
    if [ -n "$remote" ]; then
      git remote get-url "$remote" | sed 's/.*gitlab\.com:\(.*\)\.git/\1/'
    fi
  fi
}

require_command jq curl

project=$(gitlab_project_of || echo "")
if [ -n "${GCI_GL_PROJECT:-}" ]; then
  project=$GCI_GL_PROJECT
fi

branch=$(git_branch || echo "")
if [ -n "${BRANCH:-}" ]; then
  branch=$BRANCH
fi

gl_access_token=${GCI_GL_ACCESS_TOKEN:-}
env=""
dry_run=""

if [ $# = 0 ]; then
  usage
fi

while [ $# -gt 0 ]; do
  case "$1" in
  "-t" | "--gl-access-token")
    gl_access_token="$2"
    shift
    ;;
  "-p" | "--gl-project")
    project="$2"
    shift
    ;;
  "-b" | "--branch")
    branch="$2"
    shift
    ;;
  "-e" | "--env")
    env="$2"
    shift
    ;;
  "-d" | "--dry-run")
    dry_run="true"
    ;;
  "-h" | "--help")
    usage
    ;;

  '-' | *=*)
    break
    ;;

  *)
    echo "Unrecognized argument '$1', see --help."
    exit 1
    ;;

  esac
  shift
done

if [ -z "$gl_access_token" ]; then
  echo "GitLab token has not been supplied through --gl-access-token or GCI_GL_ACCESS_TOKEN. See $0 --help."
  exit 1
fi

if [ -z "$project" ]; then
  echo "GitLab project has not been supplied through --gl-project or GCI_GL_PROJECT, and could not be inferred. See $0 --help."
  exit 1
fi

if [ -z "$branch" ]; then
  echo "Target branch has not been supplied through --branch or BRANCH, and could not be inferred. See $0 --help."
  exit 1
fi

if [ -n "$env" ]; then
  environments_file=$(mktemp)
  $GCI show environments --format json > "$environments_file"
  if ! jq empty < "$environments_file"; then
    echo "An invalid environment file was generated. This might indicate that your GCI setting (currently, '$GCI') is invalid. Output: "
    cat "$environments_file"
    exit 1
  fi
  env_variables=$(jq --arg env "$env" '.environments[$env]|del(.__changes)' < "$environments_file")
  if [ "$env_variables" = "null" ]; then
    all_envs=$(jq -r '.environments|keys[]' < "$environments_file")
    echo "There is no environment '$env' defined. Defined environments are:"
    echo ""
    echo "$all_envs" | sed "s/^/ - /"
    exit 1
  fi
else
  env_variables="{}"
fi

if [ $# -gt 0 ]; then
  while [ $# -gt 0 ]; do
    # "$1" equals '-': stop processing
    if [ "${1}" = "-" ]; then
      break
    fi
    # "$1" does not contain '='
    if [ "${1#*=}" = "$1" ]; then
      usage
    fi
    key=$(echo "$1" | cut -d'=' -f1)
    value=$(echo "$1" | cut -d'=' -f2-)
    env_variables=$(
      echo "$env_variables" |
        jq --arg key "$key" \
          --arg value "$value" \
          '.[$key]=$value'
    )
    shift
  done
fi

data=$(jq -n --arg branch "$branch" --argjson env_variables "$env_variables" '{"ref": $branch, "variables": $env_variables|to_entries}')

GITLAB_API="https://gitlab.com/api/v4"

printf "Creating pipeline on branch '%s' in project '%s'%s...\n" \
  "$branch" \
  "$project" \
  "$(if [ -n "$env" ]; then echo " in environment '$env'"; else echo ""; fi)"

if [ "$dry_run" != "true" ]; then
  response=$(curl --silent --request POST \
    --header 'Content-Type: application/json' \
    --header "Private-Token: $gl_access_token" \
    --data "$data" \
    "$GITLAB_API/projects/$(uri_encode "$project")/pipeline")

  if [ "$(echo "$response" | jq 'has("web_url")')" = "false" ]; then
    echo "Cannot execute pipeline"
    echo "$response"
    exit 1
  fi

  web_url=$(echo "$response" | jq -r ".web_url")
  pipeline_id=$(echo "$response" | jq -r ".id")

  pipeline_status=$(echo "$response" | jq -r '.status')
  if [ -z "$pipeline_status" ]; then
    echo "Unable to retrieve pipeline status."
    pipeline_status='unknown'
  fi

  if [ "$web_url" != "null" ]; then
    # shellcheck disable=SC3037
    echo -n "Pipeline #$pipeline_id created at $web_url with status '$pipeline_status'."
  else
    echo "ERROR: Could not find the 'web_url' in response:"
    echo "$response" | jq
    exit 1
  fi
else
  echo curl --silent --request POST \
    --header 'Content-Type: application/json' \
    --header "Private-Token: $gl_access_token" \
    --data "$data" \
    "$GITLAB_API/projects/$(uri_encode "$project")/pipeline"
fi
