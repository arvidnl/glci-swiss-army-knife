#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ "${1:-}" = "--help" ]; then
  echo "Usage: $0 [<ci_conf_dir>]"
  echo
  echo "Simulates a pipeline in all defined environments. <ci_conf_dir> defaults to '.'"
  exit 1
fi

ci_conf_dir=${1:-.}

ignore() { true; }

header() {
  # shellcheck disable=SC3037
  echo -n "── Environment [\033[1m${1}\033[0m]  "

  range=$(seq 1 $((80 - 16 - ${#1} - 3)))
  for i in $range; do
    ignore "$i"
    # shellcheck disable=SC3037
    echo -n "─"
  done
  echo ""
}

environments_file=$(mktemp)
$GCI show environments --format json > "$environments_file"
if ! jq empty < "$environments_file"; then
  echo "An invalid environment file was generated. This might indicate that your GitLab CI Inspector setting (currently, '$GCI') is invalid. Output: "
  cat "$environments_file"
  exit 1
fi

for environment in $(jq -r '.environments|keys[]' < "$environments_file"); do
  header "$environment"

  $GCI --ci-file "$ci_conf_dir/" simulate pipeline in "$environment"

  echo
done
