#!/bin/sh

set -eu

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

# shellcheck source=./utils/gci.inc.sh
. "$SCRIPT_DIR"/gci.inc.sh

if [ -n "${TRACE:-}" ]; then set -x; fi

if [ -z "${1:-}" ] || [ "$1" = "--help" ]; then
  echo "Usage: $0 <job-name> [gitlab-runner options]"
  echo
  cat << EOT
Will focus and run the job <job-name> using gitlab-runner.
All options after <job-name> are passed to gitlab-runner.

Example:

EOT

  echo "  $0 build_x86_64 --env CI_PROJECT_PATH=tezos/tezos"
  echo
  echo "This will run the job build_x86_64 with the environment variable CI_PROJECT_PATH set to tezos/tezos."

  exit 1
fi

job=$1
shift

current_branch=$(git rev-parse --abbrev-ref HEAD)
worktree_branch="arvid@run_job--$current_branch--$(echo "$job" | tr ":" "_")-$(date +%s)"
mkdir -p /tmp/gl-exec-job/
checkout=/tmp/gl-exec-job/$worktree_branch

cleanup() {
  if [ -f "$checkout" ]; then rm -rf "$checkout"; fi
}
trap cleanup EXIT INT

# Focus job
job_yml=$(mktemp)
$GCI focus "$job" > "$job_yml"

# Create a checkout
git clone --single-branch --branch "$(git rev-parse --abbrev-ref HEAD)" -- "$(pwd)" "$checkout"
mv "$job_yml" "$checkout"

# Run job
{
  cd "$checkout"
  gitlab-runner exec docker \
    --env CI_REGISTRY=registry.gitlab.com \
    --env CI_JOB_URL="http://google.com" \
    --cicd-config-file "$(basename "$job_yml")" \
    "$@" \
    "$job"
}
