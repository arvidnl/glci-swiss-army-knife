fmt:
	ocamlformat --inplace $$(find lib_* bin tests -iname \*.ml\*)

docker-build:
	docker build . -t "registry.gitlab.com/nomadic-labs/gitlab-ci-inspector:$(shell git symbolic-ref --short HEAD)"

docker-push: docker-build
	docker push "registry.gitlab.com/nomadic-labs/gitlab-ci-inspector:$(shell git symbolic-ref --short HEAD)"
