#!/bin/bash

# shellcheck shell=bash

source /home/arvid/arvid.inc.sh
source /home/arvid/dev/nomadic-labs/tezos/scripts/util.inc.sh
source /home/arvid/dev/nomadic-labs/tezos/scripts/git.inc.sh
source /home/arvid/dev/nomadic-labs/tezos/scripts/gitlab.inc.sh

set -eu

cleanup() {
  set +e
  if [ -f LOG ]; then
    echo "failed."
    echo
    cat LOG
    rm LOG
    echo
    exit 1
  fi
}
trap cleanup EXIT INT

silent() {
  "$@" > LOG 2>&1
  rm LOG
}

gci="/home/arvid/dev/nomadic-labs/gitlab-ci-inspector/_build/default/bin/main.exe"

# current_br=$(git rev-parse --abbrev-ref HEAD)

workdir=$(mktemp -d)
br=arvid@test-exprs-"$(date +"%s")"

# git stash
# git checkout "$br"
# # git checkout "$current_br"
# git stash pop

echo "Adding worktree in $workdir with orphan branch $br"
silent git worktree add "$workdir"
silent git -C "$workdir" checkout --orphan "$br"

declare -A -g EXPECTED_JOBS
declare -A -g CI_ENVIRONMENT

ci_file="$workdir"/.gitlab-ci.yml
echo "" > $ci_file

while read -r line; do
  if echo "$line" | grep -q '^#'; then
    continue
  fi

  VAR_LINE_RE="^\([a-zA-Z_]*\)=\(.*\)"
  RULE_LINE_RE="\(.*\): '\(.*\)'\(\| # --> \(true\|false\|error\)\)"

  if echo "$line" | grep -q "$VAR_LINE_RE"; then
    var_name=$(echo "$line" | sed "s@${VAR_LINE_RE}@\1@")
    var_value=$(echo "$line" | sed "s@${VAR_LINE_RE}@\2@")
    CI_ENVIRONMENT[$var_name]=$var_value
  elif echo "$line" | grep -q "$RULE_LINE_RE"; then

    name=$(echo "$line" | sed "s@${RULE_LINE_RE}@\1@")
    rule=$(echo "$line" | sed "s@${RULE_LINE_RE}@\2@")
    result=$(echo "$line" | sed "s@${RULE_LINE_RE}@\4@")
    if [ -z "$result" ]; then
      result="unknown"
    fi

    ci_file="$workdir"/.gitlab-ci.yml
    if [ "$result" = "error" ]; then
      ci_file=${ci_file}.err
    fi

    job_name="test_${name}"

    EXPECTED_JOBS[$job_name]=$result

    cat << EOF >> "$ci_file"
$job_name:
  rules:
    - if: '${rule}'
      when: always
    - when: never
  script:
    - echo "test"
EOF

    if [ "$result" = "error" ]; then
      if $gci --ci-file "$ci_file" lint > /dev/null 2>&1; then
        echo "Expected the linting of rule $name: $rule to fail, but it did not"
        exit 1
      fi
      rm "$ci_file"
      ci_file="$workdir"/.gitlab-ci.yml
    fi
  fi
done

{
  echo "variables:"
  for var_name in "${!CI_ENVIRONMENT[@]}"; do
    var_value="${CI_ENVIRONMENT[$var_name]}"
    echo "  $var_name: $var_value"
  done
} >> "$ci_file"

echo Wrote "$workdir"/.gitlab-ci.yml
# echo
# echo with contents
# echo
# cat "$workdir"/.gitlab-ci.yml

echo "Linting"
silent gci --ci-file "$ci_file" lint

if [ "${1:-}" != "--lint" ]; then
  echo "Commiting"
  silent git -C "$workdir" -c commit.gpgsign=false commit .gitlab-ci.yml -m 'add CI configuration'

  echo "Pushing"
  silent git -C "$workdir" push --set-upstream origin "$br"

  declare -A -g ACTUAL_JOBS

  echo "Getting pipeline"
  jobs=$(mktemp)
  gitlab-get-jobs "$workdir" > "$jobs"
  while read -r job_line; do
    # echo "$job_line"
    job_name=$(echo "$job_line" | jq -r '.[0]')
    job_status=$(echo "$job_line" | jq -r '.[1]')
    ACTUAL_JOBS[$job_name]=$job_status
  done < "$jobs"

  err=0

  for job_name in "${!EXPECTED_JOBS[@]}"; do
    if [ "${EXPECTED_JOBS[$job_name]:-}" = "unknown" ]; then
      if [ -z "${ACTUAL_JOBS[$job_name]:-}" ]; then
        echo "$job_name --> false"
      else
        echo "$job_name --> true"
      fi
    fi

    if [ "${EXPECTED_JOBS[$job_name]:-}" = "true" ] && [ -z "${ACTUAL_JOBS[$job_name]:-}" ]; then
      echo "Expected the job '$job_name' to be included, but it was not!"
      err=1
    fi
  done

  for job_name in "${!ACTUAL_JOBS[@]}"; do
    if [ "${EXPECTED_JOBS[$job_name]:-}" != "true" ]; then
      echo "Did not expect the job '$job_name' to be included, but it was!"
      err=1
    fi
  done

  echo "Done!"

  exit $err
fi
