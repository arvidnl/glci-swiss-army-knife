#!/bin/sh

# Just run the docker image with some simple commands to detect
# obvious failures where the applications exits abnormally with error
# code != 0.

set -eux

image=${1:-}
pwd=$(pwd)

gcid() {
  docker run --tty -v"$pwd":"$pwd" -w"$pwd" "$image" "$@"
}

gcid man focus
gcid run-pipeline --gl-project nomadic-labs/gitlab-ci-inspector --gl-access-token glpat-... --branch arvid@phony-test-branch --dry-run FOO=BAR
