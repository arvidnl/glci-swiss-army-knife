#!/bin/sh

set -eu

if [ -n "${TRACE:-}" ]; then set -x; fi

build_test() {
  f_variant="${1:-}"
  f_docker_target="gci"
  f_image_name="${CI_REGISTRY}/${CI_PROJECT_PATH}"

  if [ -n "${f_variant}" ]; then
    f_docker_target="gci-${f_variant}"
    f_image_name="${f_image_name}/${f_variant}"
  fi

  # Sanitize branch name to valid docker tag
  BRANCH_TAG=$(echo "${CI_COMMIT_REF_NAME}" | tr -c "a-zA-Z0-9_\n" "_")
  # Login to the registry, build images and push
  docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  # In all branches, we add two tags: the short commit hash and the
  # branch name. On the default branch (master), we also add the
  # latest tag.
  # shellcheck disable=SC2046
  docker build \
    --build-arg=BUILDKIT_INLINE_CACHE=1 \
    --cache-from="${f_image_name}:latest" \
    --cache-from="${f_image_name}:${BRANCH_TAG}" \
    --build-arg OPAMSOLVERTIMEOUT="$OPAMSOLVERTIMEOUT" \
    --target="${f_docker_target}" \
    -t "${f_image_name}:${CI_COMMIT_SHORT_SHA}" \
    -t "${f_image_name}:${BRANCH_TAG}" \
    $(if [ "${CI_COMMIT_BRANCH:-}" = "$CI_DEFAULT_BRANCH" ]; then echo "-t ${f_image_name}:latest"; fi) \
    .
  if ./scripts_ci/docker-smoke-test.sh "${f_image_name}:${CI_COMMIT_SHORT_SHA}"; then
    smoke_test_pass="true"
  else
    smoke_test_pass="false"
  fi

  if [ "${smoke_test_pass}" = "true" ] || [ "${CI_COMMIT_REF_PROTECTED}" = "false" ]; then
    docker push --all-tags "${f_image_name}"
  else
    echo "Protected branch, do not push build with failed smoke test."
  fi
  if [ "${smoke_test_pass}" = "false" ]; then
    exit 1
  fi
}

build_test
build_test "tezos"
