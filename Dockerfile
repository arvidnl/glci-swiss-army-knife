FROM ocaml/opam:alpine-ocaml-4.14 as builder

WORKDIR /home/opam/gci

COPY gitlab-ci-inspector.opam gitlab-ci-inspector.opam.locked ./

# hadolint ignore=DL3018,DL3004
RUN sudo apk add --no-cache gmp-dev libev-dev && \
    sudo rm -rf /var/cache/apk/*

ARG OPAMSOLVERTIMEOUT
ENV OPAMSOLVERTIMEOUT=${OPAMSOLVERTIMEOUT:-0}

RUN opam install --deps-only . --yes --locked

COPY --chown=1000:1000 bin bin
COPY --chown=1000:1000 lib_conflang lib_conflang
COPY --chown=1000:1000 lib_stuff lib_stuff
COPY --chown=1000:1000 dune-project  .

RUN opam exec dune build bin/main.exe

# Create two versions of gci as separate layers: one vanilla versions
# with no default settings in the layer 'gci', and a version that
# preloads the tezos settings in 'gci-tezos'. We want the vanilla version to
# be the default, and so create that layer last.
FROM alpine:3.18 as gci-tezos

WORKDIR /gci

# hadolint ignore=DL3018
RUN apk --no-cache add gmp-dev libev-dev jq yq python3 py3-pip diffutils git curl && \
    apk --no-cache add icdiff --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ && \
    pip3 install --no-cache-dir csvkit==1.3.0 && \
    rm -rf /var/cache/apk/*

COPY "example_settings/environments" /gci/environments

COPY "utils" /gci/utils/

COPY "scripts/docker-entrypoint.sh" /gci/scripts/docker-entrypoint.sh

COPY --from=builder "/home/opam/gci/_build/default/bin/main.exe" /gci/bin/gitlab-ci-inspector

ENV PATH="${PATH}:/gci/bin"

ENV GCI_UTILS_DIR=/gci/utils

ENV GCI_SETTINGS=/gci/environments/gci-tezos.json

# Autoset the current working directory as safe in the entrypoint.
# This makes git work better when mapping a volume from the host into
# the container.
ENV GCI_GIT_AUTO_SAFE="true"

ENTRYPOINT ["/gci/scripts/docker-entrypoint.sh"]

# The only difference from 'gci-tezos' is that we unset the the
# tezos-specific settings file.
FROM gci-tezos as gci

ENV GCI_SETTINGS=
